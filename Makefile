PROJECT = ska-cicd-artefact-validations
HELM_RELEASE ?= ska-cicd-artefact-validations

include .make/base.mk
include .make/oci.mk
include .make/k8s.mk
include .make/python.mk
include .make/helm.mk
-include PrivateRules.mak

CHART_ENVIRONMENTS_DIR = ./charts/$(K8S_CHART)/environments

# Location of the metrics collector registry to enable multiprocessing
PROMETHEUS_MULTIPROC_DIR ?= metrics-registry

NAMESPACE_PREFIX ?= false

# This needs to be changed once the new integration tests are developed
ifeq ($(strip $(K8S_TEST)),true)
NEXUS_URL = http://nexus3-nexus-repository-manager:8081
NEXUS_API_USERNAME = admin
NEXUS_API_PASSWORD = admin123
TEST_IMAGE_TAG ?= $(shell echo $(K8S_TEST_IMAGE_TO_TEST) | rev | cut -d ":" -f 1 | rev)
TEST_IMAGE_REPOSITORY ?= $(shell echo $(K8S_TEST_IMAGE_TO_TEST) | sed 's/:$(TEST_IMAGE_TAG)$$//')
CUSTOM_VALUES += --set mongodb.replicaCount=1 --set replicaCount=1 \
--set image.repository=$(TEST_IMAGE_REPOSITORY) --set image.tag=$(TEST_IMAGE_TAG) \
--set image.pullPolicy=Always
endif

K8S_CHART_PARAMS = \
	--set global.env.gitlab_api_requester=$(GITLAB_API_REQUESTER) \
	--set global.env.gitlab_api_private_token=$(GITLAB_API_PRIVATE_TOKEN) \
	--set global.env.nexus_url=$(NEXUS_URL) \
	--set global.env.nexus_api_username=$(NEXUS_API_USERNAME) \
	--set global.env.nexus_api_password=$(NEXUS_API_PASSWORD) \
	--set global.env.mongo_db=$(MONGO_DB) \
	--set global.env.mongo_col=$(MONGO_COL) \
	--set global.env.mongo_url='$(MONGO_URL)' \
	--set global.env.redis_password=$(REDIS_PASSWORD) \
	--set global.env.unleash_proxy_url=$(UNLEASH_PROXY_URL) \
	--set global.env.unleash_proxy_sdk_token=$(UNLEASH_PROXY_SDK_TOKEN) \
	--set global.env.unleash_environment=$(UNLEASH_ENVIRONMENT) \
	--set global.env.nexus_hmac_signature_secret=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	--set global.env.harbor_url=$(HARBOR_URL) \
	--set global.env.harbor_username=$(HARBOR_USERNAME) \
	--set global.env.harbor_password=$(HARBOR_PASSWORD) \
	--set global.env.harbor_token=$(HARBOR_TOKEN) \
	--set redis.auth.password=$(REDIS_PASSWORD) \
	--set ingress.class=$(INGRESS) \
	--set mongodb.auth.rootPassword=$(MONGO_ROOT_PASSWORD) \
	--set global.storageClass='$(STORAGE)' \
	--set ingress.namespacePrefix=$(NAMESPACE_PREFIX) \
	$(CUSTOM_VALUES)

K8S_TEST_TEST_COMMAND = make MARK=$(MARK) -C resources/ test

k8s_test_kubectl_run_args = \
	$(k8s_test_runner) --restart=Never --pod-running-timeout=$(K8S_TIMEOUT) \
	--image-pull-policy=IfNotPresent --image=$(K8S_TEST_IMAGE_TO_TEST) \
	--env="SKAO_GITLAB_URL=$(SKAO_GITLAB_URL)" \
	--env="VALIDATION_WEBHOOKS_URL=$(VALIDATION_WEBHOOKS_URL)" \
	--env="NEXUS_HMAC_SIGNATURE_SECRET=$(NEXUS_HMAC_SIGNATURE_SECRET)" \
	--env="NEXUS_URL=$(NEXUS_URL)" \
	--env="NEXUS_API_USERNAME=$(NEXUS_API_USERNAME)" \
	--env="NEXUS_API_PASSWORD=$(NEXUS_API_PASSWORD)" \
	--env="CELERY_BROKER=$(CELERY_BROKER)" \
	--env="MONGO_URL=$(MONGO_URL)" \
	--env="MONGO_DB=$(MONGO_DB)" \
	--env="MONGO_COL=$(MONGO_COL)" \
	--env="GITLAB_API_PRIVATE_TOKEN=$(GITLAB_API_PRIVATE_TOKEN)" \
	--env="GITLAB_API_REQUESTER=$(GITLAB_API_REQUESTER)" \
	--env="HARBOR_TOKEN=$(HARBOR_TOKEN)" \
	$(PROXY_VALUES)


PYTHON_VARS_BEFORE_PYTEST = \
	SKAO_GITLAB_URL=$(SKAO_GITLAB_URL) \
	SKA_TRIVY_IMAGE=$(SKA_TRIVY_IMAGE) \
	SKA_GEMNASIUM_IMAGE=$(SKA_GEMNASIUM_IMAGE) \
	NEXUS_API_USERNAME=$(NEXUS_API_USERNAME) \
	NEXUS_API_PASSWORD=$(NEXUS_API_PASSWORD) \
	NEXUS_DOCKER_QUARANTINE_REPO=$(NEXUS_DOCKER_QUARANTINE_REPO) \
	NEXUS_HMAC_SIGNATURE_SECRET=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	MONGO_URL=testing MONGO_DB=$(MONGO_DB) \
	MONGO_COL=$(MONGO_COL) \
	MAIN_DOCKER_REGISTRY_HOST="" \
	UNLEASH_INACTIVE="true" \
	GITLAB_API_REQUESTER=$(GITLAB_API_REQUESTER) \
	GITLAB_API_PRIVATE_TOKEN=$(GITLAB_API_PRIVATE_TOKEN) \
	HARBOR_TOKEN=$(HARBOR_TOKEN) \
	OPENID_SECRET=$(OPENID_SECRET) \
	PYTHONPATH=${PYTHONPATH}:.:./src \
	PROMETHEUS_MULTIPROC_DIR=${PROMETHEUS_MULTIPROC_DIR}

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment' --disable-pytest-warnings --log-level=WARNING

PYTHON_SWITCHES_FOR_PYLINT = \
	--disable "invalid-name,broad-exception-raised,broad-exception-caught" \
	--min-public-methods 0 \
	--max-attributes 10 \
	--max-locals 25 \
	--max-args 8 \
	--max-positional-arguments 7
