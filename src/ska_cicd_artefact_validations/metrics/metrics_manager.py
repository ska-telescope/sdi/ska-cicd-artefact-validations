"""
Module for managing the metrics reported by the Prometheus Exporter.

This module provides the `MetricsManager` class, which is responsible for
initializing and managing the metrics used within the application, utilizing
Prometheus for metrics collection and reporting.

Classes:
    MetricsManager: A singleton class that initializes and manages all metrics
        related to artefact checks, exceptions, and uploaded artefacts.

Functions:
    check_metrics_toggle: A decorator used to conditionally execute functions
        based on feature flag settings.
"""

import os
from typing import TypeVar

from prometheus_client import (
    CollectorRegistry,
    Counter,
    Summary,
    generate_latest,
    multiprocess,
)

from ska_cicd_artefact_validations.core.config import ConfigLoader
from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.core.utils import (
    Singleton,
    check_metrics_toggle,
    metrics_feature_flag,
)
from ska_cicd_artefact_validations.metrics.metrics_config import MetricsConfig

T = TypeVar("T", bound=MetricsConfig)


class MetricsManager(metaclass=Singleton):
    """Singleton class that groups all the metrics."""

    __allow_reinitialization = False

    def __init__(self):
        self.feature_toggle = metrics_feature_flag
        self.config: T = ConfigLoader().load(MetricsConfig)

        logging.debug("Metrics configuration %s", self.config)

        os.environ["PROMETHEUS_MULTIPROC_DIR"] = (
            self.config.prometheus_multiproc_dir
        )

        if not os.path.exists(self.config.prometheus_multiproc_dir):
            os.mkdir(self.config.prometheus_multiproc_dir)

        self.registry = CollectorRegistry()
        multiprocess.MultiProcessCollector(self.registry)

        self.checks_runtime_summary = Summary(
            "check_runtime_seconds",
            "Time spent running a check.",
            ["check_name"],
        )

        self.checks_runtime_summary.labels(check_name="CheckArtefactName")
        self.checks_runtime_summary.labels(check_name="CheckArtefactTag")
        self.checks_runtime_summary.labels(check_name="CheckArtefactMetadata")
        self.checks_runtime_summary.labels(check_name="CheckRawComponentAsset")

        self.num_checks_run = Counter(
            "num_checks_run", "Number of checks run."
        )

        self.num_checks_failed = Counter(
            "num_checks_failed", "Number of checks failed.", ["check_name"]
        )

        self.num_checks_failed.labels(check_name="CheckArtefactName")
        self.num_checks_failed.labels(check_name="CheckArtefactTag")
        self.num_checks_failed.labels(check_name="CheckArtefactMetadata")
        self.num_checks_failed.labels(check_name="CheckRawComponentAsset")

        self.num_uploaded_artefacts = Counter(
            "num_uploaded_artefacts", "Number of uploaded artefacts."
        )

        self.num_exceptions = Counter(
            "num_exceptions",
            "Number of raised exceptions.",
            ["function_name"],
        )

        self.num_exceptions.labels(function_name="insertdb_task")
        self.num_exceptions.labels(function_name="check_vulnerability_report")

    @check_metrics_toggle
    def increment_counter(
        self, counter: Counter, amount: int = 1, **labels
    ) -> None:
        """
        Increments a Prometheus counter by the specified amount.

        This method uses labels if provided to increment specific instances of
        the counter.

        :param counter: The Prometheus counter to increment.
        :param amount: The amount by which to increment the counter.
        :param labels: Optional labels for incrementing specific instances of
            the counter.
        """
        if labels:
            counter.labels(**labels).inc(amount)
        else:
            counter.inc(amount)

    @check_metrics_toggle
    def record_runtime(
        self, summary: Summary, runtime: float, **labels
    ) -> None:
        """
        Records the runtime of a check in a Prometheus summary.

        This method uses labels if provided to record the runtime of specific
        instances of the summary.

        :param summary: The Prometheus summary to record the runtime in.
        :param runtime: The runtime value to be recorded.
        :param labels: Optional labels for recording specific instances of the
            summary.
        """
        if labels:
            summary.labels(**labels).observe(runtime)
        else:
            summary.observe(runtime)

    def get_metrics(self) -> None:
        """
        Generates the latest metrics data from the Prometheus registry.

        :return: The latest metrics data in a format suitable for Prometheus.
        """
        return generate_latest(self.registry)


metrics = MetricsManager()
