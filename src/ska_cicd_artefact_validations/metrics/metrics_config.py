"""
Module for defining the configuration settings for metrics.

This module provides the `MetricsConfig` class, which handles configuration
settings related to metrics, specifically for Prometheus multi-process
directory.

Classes:
    MetricsConfig: A class for storing metrics configuration settings.
"""

from pydantic import Field

from ska_cicd_artefact_validations.core.config import BaseConfig


class MetricsConfig(BaseConfig):
    """
    A class for storing metrics configuration settings.

    This class extends the `BaseConfig` and includes settings related to
    metrics, such as the directory for Prometheus multi-process mode.

    Attributes:
        _config_key: The key used to identify the metrics configuration in the
            overall configuration structure.
        prometheus_multiproc_dir: The directory path used by Prometheus for
            multi-process mode.
    """

    _config_key = "metrics"
    prometheus_multiproc_dir: str = Field(default="metrics-registry")
