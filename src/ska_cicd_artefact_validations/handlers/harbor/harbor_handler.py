"""
Module for handling interactions with Harbor artifact repositories.

This module provides the `HarborHandler` class for processing requests related
to Harbor repositories, including running checks, promoting artifacts, adding
labels, and checking vulnerability reports.

Classes:
    HarborHandler: A class for handling requests related to Harbor repositories

Enums:
    Vulnerability: An enumeration representing vulnerability severity levels.
"""

import time
from typing import Union

from ska_cicd_services_api.harbor_api import HarborApi

from ska_cicd_artefact_validations.core.artefact import (
    Artefact,
    ArtefactType,
    Vulnerability,
)
from ska_cicd_artefact_validations.core.check import run_checks
from ska_cicd_artefact_validations.core.handler import BaseHandler
from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.handlers.harbor.harbor_config import (
    HarborConfig,
)
from ska_cicd_artefact_validations.metrics.metrics_manager import metrics


class HarborHandler(BaseHandler):
    """
    A class for handling requests related to Harbor repositories.

    This class provides methods to process artefact requests, run compliance
    checks, manage labels, handle vulnerabilities, and promote artefacts in
    Harbor repositories.

    Attributes:
        config: The Harbor configuration settings loaded from `HarborConfig`.
        api: An instance of `HarborApi` for interacting with the Harbor
            repository.
        check_vulnerabilities: A boolean indicating if vulnerability checks
            should be performed based on the feature toggle.
    """

    def __init__(self, feature_toggler) -> None:
        super().__init__(HarborConfig)
        logging.debug("Harbor Handler configuration: %s", self.config)
        self.config: HarborConfig
        self.api = HarborApi()
        self.check_vulnerabilities = feature_toggler.is_enabled(
            "check-critical-vulnerabilities"
        )

    async def process_request(self, request_body: dict) -> Artefact:
        """
        Processes a request by running checks, managing labels, and promoting
        artefacts.

        This method handles incoming requests related to Harbor artefacts,
        including compliance checks, vulnerability assessments, and promotion
        to specified repositories.

        :param request_body: The request body containing artefact details.
        :return: An `Artefact` object with updated status and check results.
        """
        artefact = await self._get_artefact_data(request_body)

        results = await run_checks(artefact)
        artefact.checks_results = results

        failed_checks = []
        for check_result in artefact.checks_results:
            if not check_result.result:
                check_name = check_result.name
                failed_checks.append(
                    check_name.replace("CheckArtefact", "Invalid")
                )

        await self._add_labels(artefact, failed_checks)

        vulnerable, artefact.report = await self._check_vulnerability_report(
            artefact
        )

        if vulnerable:
            logging.warning(
                "Artefact %s:%s is vulnerable.", artefact.name, artefact.tag
            )

        if not failed_checks and (
            not vulnerable or not self.check_vulnerabilities
        ):
            await self._promote(
                artefact,
                self.config.promotion_repository,
                delete_source=self.config.delete,
            )

        return artefact

    async def _get_artefact_data(self, request_body: dict) -> Artefact:
        """
        Retrieves artefact data from Harbor based on the request details.

        :param request_body: The request body containing artefact details.
        :return: An `Artefact` object populated with data from Harbor.
        """
        artefact = Artefact()
        artefact.project = request_body["event_data"]["repository"][
            "namespace"
        ]
        artefact.name = request_body["event_data"]["repository"]["name"]
        artefact.tag = request_body["event_data"]["resources"][0]["tag"]
        artefact.type = ArtefactType.OCI

        try:
            artefact_data = await self.api.get_artifact(
                artefact.project, artefact.name, artefact.tag
            )
        except Exception as e:
            logging.error("Could not get artefact from Harbor.")
            return e

        try:
            artefact.metadata = artefact_data.extra_attrs.config.get(
                "Labels", {}
            )
        except Exception:
            logging.warning(
                "Artefact %s:%s does not contain metadata.",
                artefact.name,
                artefact.tag,
            )
            artefact.metadata = {}

        return artefact

    async def _promote(
        self, artefact: Artefact, target_project: str, delete_source=False
    ) -> None:
        """
        Promotes an artefact to the target repository and optionally deletes
        the source artefact.

        :param artefact: The artefact to be promoted.
        :param target_project: The target repository for promotion.
        :param delete_source: A flag indicating whether to delete the source
            artefact after promotion.
        """
        try:
            await self.api.copy_artifact(
                target_project,
                artefact.name,
                f"{artefact.project}/{artefact.name}:{artefact.tag}",
            )
            logging.info(
                "Artefact %s:%s promoted to %s repository.",
                artefact.name,
                artefact.tag,
                self.config.promotion_repository,
            )
        except Exception as e:
            logging.error(
                "Failed to copy artefact %s:%s to %s repository: %s",
                artefact.name,
                artefact.tag,
                target_project,
                str(e),
            )

        if not delete_source:
            logging.info(
                "Artefact %s:%s not deleted from %s",
                artefact.name,
                artefact.tag,
                artefact.project,
            )
            return

        try:
            await self.api.delete_artifact(
                artefact.project, artefact.name, artefact.tag
            )

            logging.info(
                "Artefact %s:%s deleted from %s",
                artefact.name,
                artefact.tag,
                artefact.project,
            )
        except Exception as e:
            logging.error(
                "Failed to delete artefact %s from %s repository: %s",
                artefact.name,
                artefact.project,
                str(e),
            )

    async def _add_label(self, artefact: Artefact, label_name: str) -> None:
        """
        Adds a label to the artefact in Harbor.

        This method checks if the label exists, creates it if necessary, and
        applies it to the artefact.

        :param artefact: The artefact to which the label will be added.
        :param label_name: The name of the label to be added.
        """
        # Check if label already exists in the project
        labels = await self.api.get_labels(name=label_name, scope="g")
        if not labels:
            # Create the new label
            logging.info("Creating a new label: %s", label_name)
            await self.api.create_label(label_name, scope="g")
            labels = await self.api.get_labels(name=label_name, scope="g")

        label = labels[0]

        try:
            await self.api.add_artifact_label(
                artefact.project,
                artefact.name,
                artefact.tag,
                label.model_dump(mode="json"),
            )
        except Exception:
            logging.warning("Label %s already added.", label.name)

    async def _add_labels(self, artefact: Artefact, labels: list[str]) -> None:
        """
        Adds multiple labels to the artefact in Harbor.

        :param artefact: The artefact to which the labels will be added.
        :param labels: A list of label names to be added to the artefact.
        """
        for label in labels:
            await self._add_label(artefact, label)

    async def _check_vulnerability_report(
        self, artefact: Artefact
    ) -> Union[bool, dict]:
        """
        Checks the vulnerability report of the artefact.

        This method retrieves the vulnerability report from Harbor, evaluates
        its severity, and determines if the artefact is vulnerable based on the
        configured severity threshold.

        :param artefact: The artefact for which the vulnerability report is
            checked.
        :return: A tuple containing a boolean indicating vulnerability status
            and the vulnerability report as a dictionary.
        """
        retries = 0
        while retries < self.config.query_retries:
            try:
                report = await self.api.get_artifact_vulnerabilities(
                    artefact.project, artefact.name, artefact.tag
                )
                vulnerability_report = report.vulnerabilities
                break
            except Exception:
                retries += 1
                if retries == self.config.query_retries:
                    vulnerability_report = {"vulnerability_report": []}
                    return True, vulnerability_report

                logging.warning(
                    "Vulnerability report is not yet compiled."
                    "Retrying in %ss.",
                    str(self.config.query_retry_delay_s),
                )
                metrics.increment_counter(
                    metrics.num_exceptions,
                    amount=1,
                    function_name="check_vulnerability_report",
                )
                time.sleep(self.config.query_retry_delay_s)

        report_severity = getattr(Vulnerability, str(report.severity.value))
        logging.info(
            "Artefact %s:%s vulnerability severity: %s",
            artefact.name,
            artefact.tag,
            report_severity,
        )

        if report_severity < getattr(
            Vulnerability, self.config.vulnerability_severity
        ):
            vulnerable = False
        else:
            vulnerable = True

        vulnerability_report = {
            "vulnerability_report": [
                report.model_dump_json() for report in vulnerability_report
            ]
        }

        return vulnerable, vulnerability_report
