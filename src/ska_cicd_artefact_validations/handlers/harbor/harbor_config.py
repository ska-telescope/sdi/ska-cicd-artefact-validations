"""
Module for defining the configuration settings for Harbor.

This module provides the `HarborConfig` class, which handles configuration
settings related to Harbor operations, including promotion settings, query
retries, and vulnerability severity levels.

Classes:
    HarborConfig: A class for storing Harbor configuration settings.
"""

from pydantic import Field

from ska_cicd_artefact_validations.core.config import BaseConfig


class HarborConfig(BaseConfig):
    """
    A class for storing Harbor configuration settings.

    This class extends the `BaseConfig` and includes settings related to
    Harbor operations, such as promotion repository, query retries, and
    vulnerability severity.

    Attributes:
        _config_key: The key used to identify the Harbor configuration in the
            overall configuration structure.
        promotion_repository: The name of the repository to which artefacts
            are promoted.
        query_retries: The number of retries allowed for query operations.
        query_retry_delay_s: The delay in seconds between query retries.
        vulnerability_severity: The severity level of vulnerabilities that
            trigger actions.
        promote: A boolean indicating whether artefacts should be promoted.
        delete: A boolean indicating whether artefacts should be deleted.
    """

    _config_key = "harbor"
    promotion_repository: str = Field(default="production")
    query_retries: int = Field(default=10)
    query_retry_delay_s: int = Field(default=10)
    vulnerability_severity: str = Field(default="Critical")
    promote: bool = Field(default=True)
    delete: bool = Field(default=True)
