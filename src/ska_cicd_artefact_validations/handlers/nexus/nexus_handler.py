"""
Module for handling interactions with Nexus artifact repositories.

This module provides a class `NexusHandler` for processing requests related to
Nexus repositories, including running checks, quarantining artifacts, and
deleting artefacts.

Classes:
    NexusHandler: A class for handling requests related to Nexus repositories.
"""

import re

import aiohttp

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.check import run_checks
from ska_cicd_artefact_validations.core.handler import BaseHandler
from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.handlers.nexus.artefact_handlers import (
    NexusArtefactHandler,
)
from ska_cicd_artefact_validations.handlers.nexus.nexus_config import (
    NexusConfig,
)


class NexusHandler(BaseHandler):
    """
    A handler class for managing Nexus repository artefact operations.

    This class handles artefact operations in Nexus repositories, including
    downloading assets, retrieving metadata, running validation checks, and
    moving non-compliant artefacts to quarantine.

    Attributes:
        config: The Nexus configuration settings loaded from `NexusConfig`.
        feature_toggler: A feature toggle manager for enabling or disabling
            specific features.
    """

    def __init__(self, feature_toggler) -> None:
        super().__init__(NexusConfig)
        logging.debug("Nexus Handler configuration: %s", self.config)
        self.config: NexusConfig
        self.feature_toggler = feature_toggler

    def _create_artefact_from_request(self, request_body: dict) -> Artefact:
        """
        Created an Artefact object based on the request data

        :param request_body: The request body containing artefact details.
        :return: The processed Artefact object, data extracted from the
            request body
        """
        artefact = Artefact()
        artefact.project = request_body["repositoryName"]
        artefact.name = request_body["component"]["name"].replace(
            ".tar.gz", ""
        )
        artefact.type = ArtefactType[
            request_body["component"]["format"].upper()
        ]
        artefact.id = request_body["component"]["componentId"]
        artefact.tag = request_body["component"].get("version", "no-version")
        artefact.group = request_body["component"].get("group")
        artefact.channel = request_body["component"].get("channel")

        # Raw artefacts don't have a version field in the hook
        # so we extract it from the name
        if artefact.type == ArtefactType.RAW:
            parsed_name = re.search(r"^(.*)-[^-]*\.", artefact.name).group(1)
            artefact.tag = artefact.name.replace(parsed_name + "-", "")
            artefact.name = parsed_name

        return artefact

    async def process_request(self, request_body: dict) -> Artefact:
        """
        Processes a request to handle an artefact according to its type and
        operational requirements.

        This method downloads assets, searches for metadata, and runs
        validation checks. Non-compliant artefacts are moved to quarantine.

        :param request_body: The request body containing artefact details.
        :return: The processed Artefact object, potentially modified by
            download, metadata retrieval, and checks.
        """
        artefact = self._create_artefact_from_request(request_body)

        async with aiohttp.ClientSession() as session:
            # Get the appropriate handler, depending on the type of artefact
            handler = NexusArtefactHandler(artefact, session)

            artefact.assets = await handler.download(artefact)
            artefact.metadata = await handler.get_metadata(artefact)
            artefact.checks_results = await run_checks(artefact)

            # If checks failed, quarantine the artefact
            if not artefact.is_compliant():
                quarantine_repo = (
                    artefact.project.split("-")[0] + "-quarantine"
                )
                await handler.upload(artefact, quarantine_repo)
                logging.warning(
                    "Artefact %s:%s is not compliant. Moving to %s.",
                    artefact.name,
                    artefact.tag,
                    quarantine_repo,
                )
                if self.config.delete:
                    await handler.delete(artefact)
                    logging.warning(
                        "Deleting artefact %s:%s from %s.",
                        artefact.name,
                        artefact.tag,
                        artefact.project,
                    )

            await handler.delete_downloaded_components(artefact)

            return artefact
