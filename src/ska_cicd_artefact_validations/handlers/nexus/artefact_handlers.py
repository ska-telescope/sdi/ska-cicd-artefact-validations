"""
Module for handling artefact operations with Nexus repositories.

This module provides classes and handlers for managing artefact operations in
Nexus, including downloading, uploading, deleting, and extracting metadata.
The operations are managed based on the artefact type.

Classes:
    AssetDownloader: Handles downloading of assets from Nexus.
    BaseHandler: Base class providing common functionality for Nexus API
        interactions.
    PypiHandler: Handles artefact operations specific to PyPI packages.
    HelmHandler: Handles artefact operations specific to Helm charts.
    RawHandler: Handles artefact operations specific to raw artefacts.
    ConanHandler: Handles artefact operations specific to Conan packages.
    NexusArtefactHandler: Manages artefact operations based on the type of
        artefact.
"""

import os
import tarfile
from abc import abstractmethod
from typing import Dict, List, Optional
from zipfile import ZipFile

import aiohttp
from ska_cicd_services_api.nexus_api import NexusApi

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.logging import logging


class AssetDownloader:
    """
    Handles downloading of assets from Nexus.

    This class downloads assets for a given component ID using the Nexus API
    client and stores them in a specified directory.

    Attributes:
        ASSETS_FOLDER: The folder where downloaded assets are stored.
    """

    ASSETS_FOLDER = "/tmp/assets"

    def __init__(self, api: NexusApi, session: aiohttp.ClientSession) -> None:
        self.api = api
        self.session = session
        self._chunk_size = 10 * 1024  # Bytes to read in one go
        if not os.path.exists(AssetDownloader.ASSETS_FOLDER):
            os.makedirs(AssetDownloader.ASSETS_FOLDER)

    async def download(self, component_id: str) -> List[str]:
        """
        Downloads assets for a given component ID and returns their file paths.

        :param component_id: The ID of the component whose assets are to be
            downloaded.
        :return: A list of file paths to the downloaded assets.
        """
        component = await self.api.get_component_info(component_id)
        assets_filepaths = []
        for asset in component["assets"]:
            download_url = asset["downloadUrl"]
            asset_name = download_url.split("/")[-1]
            resp = await self.session.get(download_url)

            with open(
                f"{AssetDownloader.ASSETS_FOLDER}/{asset_name}", "wb"
            ) as fd:
                while True:
                    chunk = await resp.content.read(self._chunk_size)
                    if not chunk:
                        break
                    fd.write(chunk)
                assets_filepaths.append(fd.name)

        return assets_filepaths


class BaseHandler:
    """
    Base class for artefact handlers, providing common functionality for Nexus
    API interactions.

    This class defines common operations for handling artefacts, including
    downloading, uploading, deleting, and extracting metadata.

    Attributes:
        session: An aiohttp client session for making HTTP requests.
        api: An instance of the Nexus API client.
    """

    def __init__(self, session: aiohttp.ClientSession) -> None:
        """Initializes the base handler with a session."""
        self.session = session
        self.api = NexusApi(session)
        self.upload_params = lambda fd: {"asset": fd}

    @abstractmethod
    def _set_upload_param(self):
        """
        Sets parameters for uploading assets, to be implemented by subclasses.

        :return: A dictionary of upload parameters.
        """
        return {}

    async def download(self, artefact: Artefact) -> List[str]:
        """
        Downloads assets for the given artefact.

        :param artefact: The artefact whose assets are to be downloaded.
        :return: A list of file paths to the downloaded assets.
        """
        downloader = AssetDownloader(self.api, self.session)
        return await downloader.download(artefact.id)

    async def upload(self, artefact: Artefact, target_repository: str):
        """
        Uploads artefact assets to the specified repository.

        :param artefact: The artefact whose assets are to be uploaded.
        :param target_repository: The target repository for the upload.
        :return: True if all uploads are successful, otherwise False.
        """
        if not artefact.assets:
            return None

        self._set_upload_param()
        results = []
        for asset in artefact.assets:
            with open(asset, "rb") as fd:
                result = await self.api.upload_component(  # pylint: disable=unexpected-keyword-arg # noqa: E501
                    repository_type=artefact.type.value,
                    repository=target_repository,
                    **self.upload_params(fd),
                )
                results.append(result)

        return all(results)

    async def delete(self, artefact: Artefact):
        """
        Deletes the artefact from the repository.

        :param artefact: The artefact to be deleted.
        :return: The result of the deletion operation.
        """
        return await self.api.remove_component(artefact.id)

    async def delete_downloaded_components(self, artefact: Artefact) -> None:
        """
        Deletes downloaded assets for the artefact.

        :param artefact: The artefact whose downloaded assets are to be
            deleted.
        """
        for asset in artefact.assets:
            os.remove(asset)

    async def get_metadata(self, artefact: Artefact) -> Optional[Dict]:
        """
        Extracts and returns metadata from the artefact assets.

        :param artefact: The artefact whose metadata is to be extracted.
        :return: A dictionary of extracted metadata, or None if extraction
            fails.
        """
        metadata_tar = {}
        metadata_whl = {}
        for asset in artefact.assets:
            if asset.endswith((".tar.gz", ".tgz")):
                metadata_tar = self._extract_metadata_from_tar(asset)
            elif asset.endswith(".whl"):
                metadata_whl = self._extract_metadata_from_zip(asset)

        # Returns metadata if both are the same (including both being {})
        if metadata_tar == metadata_whl:
            return metadata_tar
        # Return {} if both are different and not {}
        if metadata_tar != {} and metadata_whl != {}:
            return {}
        # Returns whichever is not None, or None if both are
        return metadata_tar if metadata_tar else metadata_whl

    def _extract_metadata_from_tar(self, tar_path) -> Optional[Dict[str, str]]:
        """
        Extracts metadata from a tar file.

        :param tar_path: The path to the tar file.
        :return: A dictionary of extracted metadata, or None if extraction
            fails.
        """
        try:
            with tarfile.open(tar_path, "r") as archive:
                files = [file.path for file in list(archive.getmembers())]
                manifest_found = False
                for file in files:
                    if "MANIFEST" in file:
                        target_file_name = file
                        manifest_found = True
                        break

                if not manifest_found:
                    raise FileNotFoundError("MANIFEST file not found.")

                with archive.extractfile(target_file_name) as file:
                    metadata = file.read().decode("utf-8").split("\n")
                    metadata = dict(
                        line.rstrip().split("=")
                        for line in metadata
                        if "=" in line
                    )
                    return metadata

        except Exception as e:
            logging.error(f"Failed to extract from tar file {tar_path}: {e}")
            return None

    def _extract_metadata_from_zip(self, zip_path) -> Optional[Dict[str, str]]:
        """
        Extracts metadata from a zip file.

        :param zip_path: The path to the zip file.
        :return: A dictionary of extracted metadata, or None if extraction
            fails.
        """
        try:
            with ZipFile(zip_path, "r") as zip_file:
                files = list(list(zip_file.namelist()))
                manifest_found = False
                for file in files:
                    if "MANIFEST" in file:
                        target_file_name = file
                        manifest_found = True
                        break

                if not manifest_found:
                    raise FileNotFoundError("MANIFEST file not found.")

                with zip_file.open(target_file_name) as file:
                    metadata = file.read().decode("utf-8").split("\n")
                    metadata = dict(
                        line.rstrip().split("=")
                        for line in metadata
                        if "=" in line
                    )
                    return metadata

        except Exception as e:
            logging.error(f"Failed to extract from zip file {zip_path}: {e}")
            return None


class PypiHandler(BaseHandler):
    """
    Handles artefact operations specific to PyPI packages.

    This handler sets parameters for uploading PyPI assets.
    """

    def _set_upload_param(self) -> None:
        """
        Sets upload parameters specific to PyPI packages.
        """
        self.upload_params = lambda fd: {"pypi_asset": fd}


class HelmHandler(BaseHandler):
    """
    Handles artefact operations specific to Helm charts.

    This handler sets parameters for uploading Helm chart assets.
    """

    def _set_upload_param(self) -> None:
        """
        Sets upload parameters specific to Helm charts.
        """
        self.upload_params = lambda fd: {"helm_asset": fd}


class RawHandler(BaseHandler):
    """
    Handles artefact operations specific to raw artefacts.

    This handler sets parameters for uploading raw artefact assets.
    """

    def _set_upload_param(self) -> None:
        """
        Sets upload parameters specific to raw artefacts.
        """
        self.upload_params = lambda fd: {
            "raw_directory": "/",
            "raw_asset1_filename": fd.name,
            "raw_asset1": fd,
        }


# pylint: disable=abstract-method
class ConanHandler(BaseHandler):
    """
    Handles artefact operations specific to Conan packages.

    This handler manages Conan-specific authentication, installation, and
    upload operations.
    """

    async def __add_remote(self, repository):
        """
        Adds a Conan remote repository.

        :param repository: The name of the Conan repository to add.
        :raises Exception: If the repository cannot be added.
        """
        add_command = f"conan remote add {repository} {os.getenv('NEXUS_URL')}/repository/{repository} --force"  # pylint: disable=line-too-long # noqa: E501
        if os.system(add_command) != 0:
            raise Exception("Couldn't add the conan remote repository.")

    async def __authenticate(self, repository):
        """
        Authenticates with a Conan repository.

        :param repository: The name of the Conan repository for authentication.
        :raises Exception: If authentication fails.
        """
        await self.__add_remote(repository)

        auth_command = f"conan user {os.getenv('NEXUS_API_USERNAME')} -p {os.getenv('NEXUS_API_PASSWORD')} -r {repository}"  # pylint: disable=line-too-long # noqa: E501
        if os.system(auth_command) != 0:
            raise Exception("Couldn't authenticate with the conan repository.")

    async def upload(self, artefact: Artefact, target_repository: str):
        """
        Uploads a Conan package to the target repository.

        :param artefact: The artefact to be uploaded.
        :param target_repository: The target repository for the upload.
        :return: True if the upload is successful, otherwise False.
        """
        await self.__authenticate(artefact.project)
        await self.__authenticate(target_repository)

        # Construct the conan package reference
        ref = f"{artefact.name}/{artefact.tag}@{artefact.group}/{artefact.channel}"  # pylint: disable=line-too-long # noqa: E501

        logging.debug("Installing and uploading %s conan package.", ref)

        # Install the package locally
        if os.system(f"conan install {ref} -r {artefact.project}") != 0:
            logging.error("Could not install the %s conan package.", ref)
            return False

        # Upload the package to the target repository
        if os.system(f"conan upload {ref} -r {target_repository} --all") != 0:
            logging.error("Could not upload the %s conan package.", ref)
            return False

        # Remove the installed package
        if os.system(f"conan remove {ref} --force") != 0:
            logging.warning("Failed to remove the %s conan package.", ref)

        return True


class NexusArtefactHandler:
    """
    Manages artefact operations based on the type of artefact.

    This class initializes the appropriate handler based on the artefact type
    and delegates operations like download, upload, delete, and metadata
    extraction.

    Attributes:
        handlers: A dictionary mapping artefact types to their respective
            handlers.
    """

    handlers = {
        ArtefactType.PYPI: PypiHandler,
        ArtefactType.HELM: HelmHandler,
        ArtefactType.RAW: RawHandler,
        ArtefactType.CONAN: ConanHandler,
    }

    def __init__(
        self, artefact: Artefact, session: aiohttp.ClientSession
    ) -> None:
        self.session = session
        self.handler = NexusArtefactHandler.handlers.get(artefact.type)(
            self.session
        )

    async def download(self, artefact: Artefact):
        """
        Downloads assets for the artefact.

        :param artefact: The artefact whose assets are to be downloaded.
        :return: A list of file paths to the downloaded assets.
        """
        return await self.handler.download(artefact)

    async def upload(self, artefact: Artefact, target_repository: str):
        """
        Uploads the artefact to the target repository.

        :param artefact: The artefact to be uploaded.
        :param target_repository: The target repository for the upload.
        :return: True if the upload is successful, otherwise False.
        """
        return await self.handler.upload(artefact, target_repository)

    async def delete(self, artefact: Artefact):
        """
        Deletes the artefact from the repository.

        :param artefact: The artefact to be deleted.
        :return: The result of the deletion operation.
        """
        return await self.handler.delete(artefact)

    async def get_metadata(self, artefact: Artefact):
        """
        Extracts and returns metadata from the artefact.

        :param artefact: The artefact whose metadata is to be extracted.
        :return: A dictionary of extracted metadata, or None if extraction
            fails.
        """
        return await self.handler.get_metadata(artefact)

    async def delete_downloaded_components(self, artefact: Artefact):
        """
        Deletes downloaded components associated with the artefact.

        :param artefact: The artefact whose downloaded components are to be
            deleted.
        """
        return await self.handler.delete_downloaded_components(artefact)
