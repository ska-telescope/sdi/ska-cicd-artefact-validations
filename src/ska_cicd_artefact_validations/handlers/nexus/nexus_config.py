"""
Module for defining the configuration settings for Nexus.

This module provides the `NexusConfig` class, which handles configuration
settings related to Nexus operations, including quarantine and delete options
for artefacts.

Classes:
    NexusConfig: A class for storing Nexus configuration settings.
"""

from pydantic import Field

from ska_cicd_artefact_validations.core.config import BaseConfig


class NexusConfig(BaseConfig):
    """
    A class for storing Nexus configuration settings.

    This class extends the `BaseConfig` and includes settings related to
    Nexus operations, such as whether artefacts should be quarantined or
    deleted.

    Attributes:
        _config_key: The key used to identify the Nexus configuration in the
            overall configuration structure.
        quarantine: A boolean indicating whether artefacts should be
            quarantined.
        delete: A boolean indicating whether artefacts should be deleted.
    """

    _config_key = "nexus"
    quarantine: bool = Field(default=True)
    delete: bool = Field(default=True)
