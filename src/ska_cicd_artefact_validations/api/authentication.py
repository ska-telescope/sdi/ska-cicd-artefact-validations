"""
Module: Authentication

This module provides classes for implementing various authentication methods
in a FastAPI application.

Classes:
    TokenAuthenticator: Authenticates requests based on a predefined token.
    PasswordAuthenticator: Authenticates requests using a basic
        username/password mechanism.
    HmacSignatureAuthenticator: Authenticates requests using HMAC (Hash-based
        Message Authentication Code) signatures.
    FactoryAuthenticator: Main class for handling authentication based on the
        specified authentication type.
"""

import base64
import hashlib
import hmac
import os

from fastapi import HTTPException, Request, status
from fastapi.security.api_key import APIKeyHeader


class TokenAuthenticator:
    """
    Authenticates requests based on a predefined token.

    This class validates incoming requests by checking a token present in the
    request headers against a predefined token.

    Methods:
        authenticate: Authenticates a request by comparing the provided token
            with the expected value.
    """

    def __init__(self, config) -> None:
        try:
            self.token_header = config.get("token_header_env")
            self.token = os.getenv(config.get("token_env"))
        except TypeError as exc:
            raise TypeError(
                "Token authentication fields are invalid."
            ) from exc

    async def authenticate(self, request: Request) -> None:
        """
        Authenticates the request using a token from the headers.

        :param request: The incoming request object.
        :raises HTTPException: If the token is invalid or not provided.
        """
        key_retriever = APIKeyHeader(name=self.token_header, auto_error=True)
        token = await key_retriever(request)
        if token != self.token:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid API Key",
            )


class PasswordAuthenticator:
    """
    Authenticates requests using a basic username/password mechanism.

    This class validates incoming requests by decoding a basic authentication
    header and comparing the credentials against predefined values.

    Methods:
        authenticate: Authenticates a request using basic authentication.
    """

    def __init__(self, config) -> None:
        try:
            self.username = os.getenv(config.get("username_env"))
            self.password = os.getenv(config.get("password_env"))
        except TypeError as exc:
            raise TypeError(
                "Password authentication fields are invalid."
            ) from exc

    async def authenticate(self, request: Request) -> None:
        """
        Authenticates the request using basic authentication.

        :param request: The incoming request object.
        :raises HTTPException: If the credentials are invalid or not provided.
        """
        auth_code = request.headers.get("authorization")
        if auth_code is None:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Authorization failed.",
            )

        user_password_coded = auth_code.split(" ")[1]
        assert (
            user_password_coded is not None
        ), "Bad basic authorization request"

        user_password_bytes = user_password_coded.encode("utf-8")
        user_password = base64.b64decode(user_password_bytes).decode("utf-8")

        username, password = user_password.split(":")

        if username != self.username or password != self.password:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid username and/or password",
            )


class HmacSignatureAuthenticator:
    """
    Authenticates requests using HMAC (Hash-based Message Authentication Code)
    signatures.

    This class validates incoming requests by generating an HMAC signature of
    the request body and comparing it to the signature provided in the request
    headers.

    Methods:
        authenticate: Authenticates a request using an HMAC signature.
        generate_hmac_signature: Generates an HMAC signature for the given data
            and secret.
    """

    def __init__(self, config) -> None:
        try:
            self.hmac_signature_header = config.get("hmac_signature_header")
            self.hmac_secret = os.getenv(config.get("hmac_secret_env"))
            if self.hmac_signature_header is None:
                raise TypeError(
                    "Hmac Signature authentication fields are invalid."
                )
        except TypeError as exc:
            raise TypeError(
                "Hmac Signature authentication fields are invalid."
            ) from exc

    async def authenticate(self, request: Request) -> None:
        """
        Authenticates the request using an HMAC signature.

        :param request: The incoming request object.
        :raises HTTPException: If the HMAC signature is invalid or not
            provided.
        """
        key_retriever = APIKeyHeader(
            name=self.hmac_signature_header, auto_error=True
        )
        hmac_signature = await key_retriever(request)

        rq_body = await request.body()
        secret_encoded = bytes(self.hmac_secret, "utf-8")

        digest = self.generate_hmac_signature(rq_body, secret_encoded)

        if hmac_signature != digest:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid HMAC Signature or corrupted payload received",
            )

    def generate_hmac_signature(self, data, secret) -> str:
        """
        Generates an HMAC signature for the given data and secret.

        :param data: The data to sign.
        :param secret: The secret key used to generate the signature.
        :return: The generated HMAC signature as a hexadecimal string.
        """
        digest_maker = hmac.new(secret, data, hashlib.sha1)
        return digest_maker.hexdigest()


class FactoryAuthenticator:
    """
    Handles authentication based on the specified authentication type.

    This class selects and manages the appropriate authenticator based on
    configuration settings, allowing flexible and configurable authentication
    handling.

    Methods:
        authenticate: Authenticates the request based on the configured
            authentication type.
    """

    def __init__(self, auth_config) -> None:
        self.authenticator = None
        auth_type = auth_config.get("auth_type")

        match auth_type:
            case "token":
                self.authenticator = TokenAuthenticator(auth_config)
            case "password":
                self.authenticator = PasswordAuthenticator(auth_config)
            case "hmac_signature":
                self.authenticator = HmacSignatureAuthenticator(auth_config)
            case "none":
                self.authenticator = None
            case _:
                raise ValueError(
                    "Authentication type field is invalid."
                    f" Requested type: {auth_type}"
                )

    async def authenticate(self, request: Request) -> None:
        """
        Authenticates the request using the configured authenticator.

        :param request: The incoming request object.
        """
        if self.authenticator is not None:
            await self.authenticator.authenticate(request)
