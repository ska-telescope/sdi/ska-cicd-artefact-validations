"""
Module for defining hook configurations for Nexus and Harbor.

This module provides configuration classes for handling hooks from Nexus and
Harbor, including validation of incoming data.

Classes:
    BaseHookConfig: Base configuration class allowing extra fields.
    NexusComponent: A class representing a component in a Nexus hook.
    NexusHook: A class representing a Nexus hook event with its metadata.
    HarborResource: A class representing a resource in a Harbor hook event.
    HarborEventData: A class representing the event data in a Harbor hook.
    HarborHook: A class representing a Harbor hook event with its metadata.
"""

from typing import List

from pydantic import BaseModel, ConfigDict, field_validator


class BaseHookConfig(BaseModel):
    """
    Base configuration class allowing extra fields.

    This class serves as the base for other hook configuration classes and
    allows additional fields not explicitly defined in the model.
    """

    model_config = ConfigDict(extra="allow")


class NexusComponent(BaseHookConfig):
    """
    A class representing a component in a Nexus hook.

    Attributes:
        id: The unique identifier of the component.
        componentId: The component ID in Nexus.
        format: The format of the component.
        name: The name of the component.
    """

    id: str
    componentId: str
    format: str
    name: str


class NexusHook(BaseHookConfig):
    """
    A class representing a Nexus hook event with its metadata.

    Attributes:
        timestamp: The timestamp of the event.
        nodeId: The ID of the node that generated the event.
        initiator: The user or process that initiated the event.
        repositoryName: The name of the repository involved in the event.
        action: The action that triggered the event.
        component: The component involved in the event.
    """

    timestamp: str
    nodeId: str
    initiator: str
    repositoryName: str
    action: str
    component: NexusComponent

    @field_validator("action")
    def validate_action(
        cls, value
    ):  # pylint: disable=no-self-argument # noqa: E0213
        """
        Validator to ensure the action is "CREATED".

        :param value: The action value to validate.
        :return: The validated action value.
        :raises ValidationError: If the action is not 'CREATED'.
        """
        if value != "CREATED":
            raise ValueError("Action must be CREATED.")
        return value


class HarborResource(BaseHookConfig):
    """
    A class representing a resource in a Harbor hook event.

    Attributes:
        digest: The digest of the resource.
        tag: The tag of the resource.
        resource_url: The URL of the resource.

    Methods:
        tag_must_not_start_with_sha256: Validator to ensure tags do not start
            with 'sha256' to avoid triggering on signature push events.
    """

    digest: str
    tag: str
    resource_url: str

    @field_validator("tag")
    def tag_must_not_start_with_sha256(
        cls, value
    ):  # pylint: disable=no-self-argument # noqa: E0213
        """
        Validator to ensure the tag does not start with 'sha256'.

        Harbor triggers an event when a signature is pushed, which has a tag
        starting with 'sha256'. This validator skips such triggers.

        :param value: The tag value to validate.
        :return: The validated tag value.
        :raises ValueError: If the tag starts with 'sha256'.
        """
        if value.startswith("sha256"):
            raise ValueError('tag must not start with "sha256"')
        return value


class HarborEventData(BaseHookConfig):
    """
    A class representing the event data in a Harbor hook.

    Attributes:
        resources: A list of resources involved in the event.
        repository: A dictionary containing repository information.
    """

    resources: List[HarborResource]
    repository: dict


class HarborHook(BaseHookConfig):
    """
    A class representing a Harbor hook event with its metadata.

    Attributes:
        type: The type of the event.
        occur_at: The timestamp when the event occurred.
        operator: The operator who triggered the event.
        event_data: The data related to the event, including resources and
            repository information.
    """

    type: str
    occur_at: int
    operator: str
    event_data: HarborEventData
