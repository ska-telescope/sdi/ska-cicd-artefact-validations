"""
Module for validating hooks using Pydantic models.

This module provides the `HookValidator` class, which is used to validate
incoming hook data from various sources, such as Nexus and Harbor, using
Pydantic models.

Classes:
    HookValidator: A class to validate hooks based on Pydantic models.
"""

from typing import Type

from pydantic import BaseModel, ValidationError

from ska_cicd_artefact_validations.api.hook_models import HarborHook, NexusHook
from ska_cicd_artefact_validations.core.logging import logging


class HookValidator:
    """
    A class to validate hooks based on Pydantic models.

    This class uses predefined Pydantic models to validate incoming hook data
    from supported request types, such as Nexus and Harbor.

    Attributes:
        REQUEST_TYPE: A dictionary mapping request types to their respective
            Pydantic models.
        request_type: The type of request being validated.
        model_class: The Pydantic model class used for validation.
    """

    REQUEST_TYPE = {
        "nexus": NexusHook,
        "harbor": HarborHook,
    }

    def __init__(self, request_type: str) -> None:
        self.request_type = request_type
        self.model_class: Type[BaseModel] = HookValidator.REQUEST_TYPE.get(
            request_type
        )
        if not self.model_class:
            raise ValueError(f"Unsupported request type: {request_type}")

    def is_valid(self, hook_data: dict) -> bool:
        """
        Checks if the provided hook data is valid according to the Pydantic
        model.

        This method attempts to validate the incoming hook data against the
        assigned Pydantic model and returns the validation status.

        :param hook_data: The hook data to be validated.
        :return: True if the hook data is valid, otherwise False.
        """
        try:
            self.model_class(**hook_data)
            return True
        except ValidationError as e:
            logging.error("Validation error: %s", e)
            return False
