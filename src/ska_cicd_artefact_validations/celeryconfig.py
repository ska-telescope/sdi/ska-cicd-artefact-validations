"""
Module for initializing and configuring Celery tasks and workers.

This module provides functionalities for initializing Celery tasks and
workers, including setting up the feature toggle client, configuring worker
processes, and defining Celery task settings.

Variables:
    feature_toggler: UnleashClient instance for managing feature toggles.
    imports (list): List of task module imports.
    broker_url (str): URL of the Celery broker.
    result_backend (str): URL of the result backend.
    worker_concurrency (int): Number of concurrent worker processes.
    worker_prefetch_multiplier (int): Multiplier for the number of messages to
        prefetch at a time.
    task_soft_time_limit (int): Soft time limit for Celery tasks in seconds.
    task_time_limit (int): Time limit for Celery tasks in seconds.

Functions:
    configure_workers: Function to configure worker processes during
        initialization.
"""

import os

from celery.signals import worker_process_init
from UnleashClient import UnleashClient

from ska_cicd_artefact_validations.core.logging import logging

# Feature Toggle Client
feature_toggler = UnleashClient(
    url=os.getenv("UNLEASH_PROXY_URL", default="mock-url") + "/proxy",
    app_name=os.getenv("UNLEASH_ENVIRONMENT", default="development"),
    custom_headers={
        "Authorization": os.getenv("UNLEASH_PROXY_SDK_TOKEN", "mock-id")
    },
    disable_metrics=True,
    disable_registration=True,
    verbose_log_level=logging.ERROR,  # Only Error and above
    refresh_interval=60,
    cache_directory="src/ska_cicd_artefact_validations",
)


def initialize_feature_toggler(my_feature_toggler: UnleashClient) -> None:
    """
    Initializes the UnleashClient feature toggler and
    sets log levels based on its state.
    Args:
        feature_toggler (UnleashClient): The feature toggler instance.
    """

    if "UNLEASH_INACTIVE" not in os.environ:
        try:
            my_feature_toggler.initialize_client()
            if not my_feature_toggler.is_initialized:
                my_feature_toggler.unleash_verbose_log_level = logging.NOTSET
        except Exception:  # pylint: disable=broad-exception-caught
            my_feature_toggler.unleash_verbose_log_level = logging.NOTSET

        if my_feature_toggler.is_initialized:
            my_feature_toggler.unleash_verbose_log_level = logging.ERROR


# https://docs.getunleash.io/unleash-client-python/celery.html
# ST-1296: Move initialization to worker_process_init
@worker_process_init.connect
def configure_workers(sender=None, conf=None, **kwargs):
    """
    Configure worker processes during initialization.

    This function is connected to the Celery worker process initialization
    signal and sets up the UnleashClient for feature toggling. It initializes
    the feature toggle client unless the 'UNLEASH_INACTIVE' environment
    variable is set, and adjusts the logging level based on the initialization
    status of the client.

    :param sender: The sender of the signal, usually the worker. Defaults to
        None.
    :param conf: Configuration settings for the worker. Defaults to None.
    :param kwargs: Additional keyword arguments.

    :raises Exception: If initialization of the feature toggler fails.
    """
    # pylint: disable=unused-argument
    initialize_feature_toggler(feature_toggler)


include = ["ska_cicd_artefact_validations.tasks.main_task"]
broker_url = os.getenv("CELERY_BROKER", default="redis://localhost:6379/0")
result_backend = os.getenv("CELERY_BROKER", default="redis://localhost:6379/0")
broker_connection_retry_on_startup = True
worker_concurrency = 6
worker_prefetch_multiplier = 1
task_soft_time_limit = 3600  # 1 hour
task_time_limit = 7200  # 2 hours
task_default_retry_delay = 60 * 5  # 5 minutes
