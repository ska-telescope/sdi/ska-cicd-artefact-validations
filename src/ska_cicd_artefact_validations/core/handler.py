"""
Module for base handler class used in artefact management.

This module defines the `BaseHandler` class, which provides a structure
for handling requests related to artefact management by using configuration
classes and processing artefact requests.

Classes:
    BaseHandler: Abstract base class for handling artefact management
    requests.
"""

from abc import abstractmethod
from typing import TypeVar

from ska_cicd_artefact_validations.core.artefact import Artefact
from ska_cicd_artefact_validations.core.config import BaseConfig, ConfigLoader

T = TypeVar("T", bound=BaseConfig)


class BaseHandler:
    """
    Abstract base class for handling artefact management requests.

    This class provides the structure and initial configuration setup required
    to process artefact requests. Subclasses should implement specific
    processing logic by overriding the `process_request` method.

    Methods:
        process_request(self, request_body): Abstract method to process the
            artefact request.
    """

    def __init__(self, config_class: T) -> None:
        self.config: T = ConfigLoader().load(config_class)

    @abstractmethod
    async def process_request(self, request_body: dict) -> Artefact:
        """
        Process a request related to artefact management.

        This method should be implemented by subclasses to handle specific
        processing logic for artefact requests.

        :param request_body: The request payload containing artefact details.
        Expected to include keys like 'event_data' with nested keys for
        'repository' and 'resources' to identify the artefact.
        :return: The processed artefact object with updated status, metadata,
        and check results.
        :raises Exception: If any error occurs during the processing of the
        request.
        """
