"""
Module for defining the structure of an Artefact.

This module provides the `Artefact` class, which represents a generic artefact
and includes properties for artefact details, methods for JSON serialization,
and compliance checking.

Classes:
    ArtefactType: Enum representing different types of artefacts.
    Artefact: A class for storing the data related to a generic artefact.
    Vulnerability: Enum representing different levels of vulnerability
        severity.
"""

import json
from enum import Enum, IntEnum
from typing import Any

from pydantic import BaseModel, Field


class ArtefactType(Enum):
    """
    Enum representing different types of artefacts.

    The ArtefactType enum defines various types of artefacts that can be
    managed, including OCI, PYPI, HELM, RAW, and CONAN.
    """

    OCI = "oci"
    PYPI = "pypi"
    HELM = "helm"
    RAW = "raw"
    CONAN = "conan"


class Artefact(BaseModel):
    """
    A class for storing the data related to a generic artefact.

    This class encapsulates the properties of an artefact, such as its project
    details, type, metadata, and validation results. It provides methods for
    serializing artefacts to JSON and checking their compliance status.

    Attributes:
        project: The project associated with the artefact.
        name: The name of the artefact.
        tag: The tag or version of the artefact.
        type: The type of the artefact, as defined by the ArtefactType enum.
        id: The unique identifier of the artefact.
        metadata: A dictionary containing metadata related to the artefact.
        assets: A list of assets associated with the artefact.
        checks_results: A list of dictionaries containing results of various
            checks on the artefact.
        report: A field for storing any report associated with the artefact.
        group: Specific to Conan artefacts, representing the artefact group.
        channel: Specific to Conan artefacts, representing the artefact
        channel.
    """

    project: str = Field(default=None)
    name: str = Field(default=None)
    tag: str = Field(default=None)
    type: ArtefactType = Field(default=None)
    id: str = Field(default=None)
    metadata: dict = Field(default=None)
    assets: list[str] = Field(default=None)
    checks_results: list[Any] = Field(default=[])
    report: Any = Field(default=None)

    # Conan Specific
    group: str = Field(default=None)
    channel: str = Field(default=None)

    def to_json(self) -> dict:
        """
        Serializes the artefact to a JSON-compatible dictionary.

        :return: A dictionary representation of the artefact in JSON format.
        """
        return json.loads(self.model_dump_json())

    def is_compliant(self) -> bool:
        """
        Checks if the artefact is compliant based on its checks results.

        This method evaluates all checks associated with the artefact to
        determine compliance.

        :return: True if all checks pass, otherwise False.
        """
        return all(check_result.result for check_result in self.checks_results)


class Vulnerability(IntEnum):
    """
    Enum representing different levels of vulnerability severity.

    The `Vulnerability` enum defines severity levels from Unknown to Critical
    and provides methods for comparing these levels.

    Attributes:
        Critical: Represents the highest severity level with a value of 4.
        High: Represents a high severity level with a value of 3.
        Medium: Represents a medium severity level with a value of 2.
        Low: Represents a low severity level with a value of 1.
        Unknown: Represents an unknown severity level with a value of 0.
    """

    Critical = 4
    High = 3
    Medium = 2
    Low = 1
    Unknown = 0
