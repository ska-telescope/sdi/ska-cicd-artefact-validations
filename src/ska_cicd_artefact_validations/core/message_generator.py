"""
Module for message generation and merge request execution.

This module provides classes and functions for generating messages related to
artefact validations and managing merge requests in GitLab. It includes
functionality for creating formatted messages, handling comments, and
executing merge requests using the GitLab API.

Classes:
    MessageType: Enum representing different message types (failure, warning,
        info).
    MessageGenerator: Generates formatted messages for artefact validations.
    MergeRequestExecutor: Executes actions related to merge requests in GitLab.

Functions:
    get_message_type: Retrieves the MessageType enum based on its string value.
"""

from enum import Enum
from typing import List, Optional

import prettytable
from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_artefact_validations.core.logging import logging


class MessageType(Enum):
    """
    Enum representing different types of messages.

    The MessageType enum defines symbols for failure, warning, and information
    messages, along with a method to determine their display order.
    """

    FAILURE = ":no_entry_sign:"
    WARNING = ":warning:"
    INFO = ":book:"

    def order(self):
        """
        Returns the order of the message type for sorting purposes.

        :return: An integer representing the order of the message type.
        """
        order = {"FAILURE": 3, "WARNING": 2, "INFO": 1}
        return order[self.name]


def get_message_type(enum_value: str):
    """
    Retrieves the MessageType enum based on its string value.

    :param enum_value: The string representation of the MessageType.
    :return: The corresponding MessageType enum or None if not found.
    """
    for enum in MessageType:
        if enum.value == enum_value:
            return enum

    return None


class MessageGenerator:
    """
    Generates formatted messages for artefact validations.

    This class provides methods to add pretext, entries, and posttext to
    messages, generate metadata, and retrieve complete messages formatted in
    HTML.

    Methods:
        add_entry: Adds an entry to the message table with type, description,
            and mitigation.
        generate_metadata: Generates metadata for identifying the message in
            GitLab.
        add_pretext: Adds pretext to the message.
        add_posttext: Adds posttext to the message.
        get_message: Retrieves the formatted message as a string.
        get_well_done_message: Retrieves a success message indicating all
            checks passed.
        find_comment: Finds a specific comment in the merge request comments.
        add_entry_to_comment: Adds a new entry to an existing comment.
    """

    message_marker = "ska-devsecops-mr-service-id"

    def __init__(self):
        self.pretext = ""
        self.table = prettytable.PrettyTable()
        self.table.align = "c"
        self.table.field_names = ["Type", "Description", "Mitigation Strategy"]
        self.posttext = ""

    async def add_entry(self, type_: str, description, mitigation):
        """
        Adds an entry to the message table with type, description, and
        mitigation.

        :param type: The type of the message entry (e.g., failure, warning).
        :param description: The description of the issue.
        :param mitigation: The mitigation strategy for the issue.
        """
        self.table.add_row([type_, description, mitigation])

    async def generate_metadata(self):
        """
        Generates metadata for identifying the message in GitLab.

        :return: A string containing the metadata.
        """
        metadata = [
            "<!--",
            f"MRServiceID: {MessageGenerator.message_marker}",
            "-->",
        ]
        return "\n".join(metadata)

    async def add_pretext(self, pretext):
        """
        Adds pretext to the message.

        :param pretext: The pretext to be added before the main message.
        """
        self.pretext = pretext

    async def add_posttext(self, posttext):
        """
        Adds posttext to the message.

        :param posttext: The posttext to be added after the main message.
        """
        self.posttext = posttext

    async def get_message(self):
        """
        Retrieves the formatted message as a string.

        This method generates the complete message, including pretext, table
        entries, and posttext.

        :return: A string containing the formatted message.
        """
        metadata = await self.generate_metadata()
        return "\n\n".join(
            [
                metadata,
                self.pretext,
                self.table.get_html_string(
                    sort_key=lambda x: (
                        get_message_type(x[0]).order()
                        if get_message_type(x[0]) is not None
                        else x
                    ),
                    sortby="Type",
                    reversesort=True,
                ),
                self.posttext,
            ]
        )

    async def get_well_done_message(self):
        """
        Retrieves a success message indicating all checks passed.

        :return: A string containing the well-done message.
        """
        self.table.clear_rows()
        self.table.format = True
        self.pretext = (
            ":tada: Well Done!"
            " All the merge request quality checks have passed!"
        )
        self.table.add_row(
            [":checkered_flag:", "All checks passed!", ":coffee:"]
        )
        return await self.get_message()

    async def find_comment(self, mr_comments, comment_description):
        """
        Finds a specific comment in the merge request comments.

        :param mr_comments: An iterable of comments from the merge request.
        :param comment_description: The description text to identify the
        comment.
        :return: The identified comment or None if not found.
        """

        marvin_comment = None

        async for comment in mr_comments:
            if MessageGenerator.message_marker in comment["body"]:
                if comment_description not in comment:
                    marvin_comment = comment

        return marvin_comment

    async def add_entry_to_comment(
        self,
        comment: str,
    ):
        """
        Adds a new entry to an existing comment.

        :param comment: The comment body where the new entry will be added.
        :return: The updated comment with the new entry included.
        """
        new_comment = await self.get_message()

        marvin_comment = comment["body"]

        start = "<tbody>\n"
        end = "</tbody>"
        # get only body from comment
        marvin_comment = (marvin_comment.split(start))[1].split(end)[0]
        # add existing table to new one
        index = new_comment.find(end)

        new_comment = (
            new_comment[:index] + marvin_comment + new_comment[index:]
        )

        return new_comment


class MergeRequestExecutor:
    """
    Executes actions related to merge requests in GitLab.

    This class provides methods for creating branches and merge requests in
    GitLab based on artefact validation results.

    Methods:
        action: Creates a branch and merge request in GitLab.
    """

    def __init__(self, api: GitLabApi):
        """
        Initializes the executor with a GitLab API instance.

        :param api: The GitLab API instance used to interact with GitLab.
        """
        self.api = api

    async def action(
        self,
        proj_id: int,
        source_branch: str,
        target_branch: str,
        title: str,
        description_text: str,
        assignee_ids: List[int],
    ) -> Optional[dict]:
        """
        Creates a branch and merge request in GitLab.

        This method creates a new branch and submits a merge request in GitLab
        with the provided details.

        :param proj_id: The project ID where the merge request will be created.
        :param source_branch: The source branch for the merge request.
        :param target_branch: The target branch for the merge request.
        :param title: The title of the merge request.
        :param description_text: The description of the merge request.
        :param assignee_ids: A list of user IDs to assign the merge request to.
        :return: A dictionary containing merge request details if successful,
            otherwise None.
        """
        br_result = await self.api.create_branch(
            proj_id, source_branch, target_branch
        )
        logging.info(f"Created branch: {br_result}")
        if not br_result or "name" not in br_result:
            logging.error("Branch creation failed.")
            return None

        logging.info(
            "\nID: %s\nSource Branch: %s\nTarget Branch: %s\nTitle: %s\nDescription: %s\nAssignees: %s",  # pylint: disable=line-too-long # noqa: E501
            proj_id,
            source_branch,
            target_branch,
            title,
            description_text,
            assignee_ids,
        )
        mr_result = await self.api.create_mr(
            proj_id=proj_id,
            source_branch=source_branch,
            target_branch=target_branch,
            title=title,
            description=description_text,
            assignee_ids=assignee_ids,
        )
        logging.info(f"Created merge request: {mr_result}")
        if not mr_result or "iid" not in mr_result:
            logging.error("Merge request creation failed.")
            return None

        return mr_result
