"""
Module for utilities including decorators and singleton pattern implementation.

This module provides utility functions and classes, including a decorator for
feature toggle checks and a metaclass for implementing the singleton pattern.

Functions:
    check_metrics_toggle: A decorator to conditionally execute a function based
        on a feature toggle.

Classes:
    Singleton: A metaclass that implements the singleton pattern to ensure only
        one instance of a class exists.
"""

from typing import Callable

from ska_cicd_artefact_validations.celeryconfig import feature_toggler
from ska_cicd_artefact_validations.core.logging import logging

metrics_feature_flag = "metrics"


def check_metrics_toggle(func: Callable) -> Callable:
    """
    Decorates a function to run only if the metrics feature flag is enabled.

    This decorator checks the status of the metrics feature flag before
    executing the decorated function. If the flag is disabled, the function
    will not run, and a debug message will be logged.

    :param func: The function to be decorated.
    :return: The wrapped function that includes the feature flag check.
    """

    def wrapper(*args, **kwargs):
        if feature_toggler.is_enabled(
            metrics_feature_flag,
            fallback_function=lambda feature_name, context: False,
        ):
            func(*args, **kwargs)
        else:
            logging.debug("Metrics exporter is disabled.")

    return wrapper


class Singleton(type):
    """
    Metaclass implementing the singleton pattern.

    This metaclass ensures that any class using it will have only one instance
    throughout the application. It is useful for classes where a single shared
    instance is needed.

    Attributes:
        _instances: A dictionary storing the single instances of the classes
            using this metaclass.
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Creates or returns the existing instance of the class.

        This method checks if an instance of the class already exists. If it
        does, it returns that instance; otherwise, it creates a new one.

        :param args: Arguments to pass to the class constructor.
        :param kwargs: Keyword arguments to pass to the class constructor.
        :return: The single instance of the class.
        """
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs
            )

        return cls._instances[cls]
