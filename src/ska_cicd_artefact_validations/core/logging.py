"""logging is a wrapper module to setup the logging format"""

import logging
import os

LOGGING_FORMAT = "%(asctime)s [level=%(levelname)s]: %(message)s"
logging.basicConfig(
    level=logging.getLevelName(os.environ.get("LOG_LEVEL", "INFO")),
    format=LOGGING_FORMAT,
)

logging.getLogger("UnleashClient").setLevel(logging.ERROR)
logging.getLogger("apscheduler").setLevel(logging.ERROR)
logging.getLogger("httpx").setLevel(logging.ERROR)
