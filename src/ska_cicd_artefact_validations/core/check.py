"""
Module for dynamically loading and running artefact checks.

This module provides the functionality to import, manage, and execute various
checks on artefacts. It includes abstract base classes for defining checks,
methods for dynamically importing check classes, and functions to run the
checks with feature toggle support.

Classes:
    Check: Abstract base class for defining checks.
    CheckResult: A class representing the result of a check execution.

Functions:
    import_check_classes: Dynamically imports check classes from the checks
        directory.
    perform_check: Executes a check on an artefact and returns the result.
    run_checks: Runs all imported checks on the given artefact.
"""

import asyncio
import importlib
import os
import time
from abc import ABC, abstractmethod
from typing import Dict, List, Type, Union

from pydantic import BaseModel

from ska_cicd_artefact_validations.celeryconfig import feature_toggler
from ska_cicd_artefact_validations.core.artefact import Artefact
from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.core.message_generator import MessageType
from ska_cicd_artefact_validations.metrics.metrics_manager import metrics

CHECKS_FOLDER = os.path.join(os.path.dirname(__file__), "..", "checks")
MODULE_PATH = "ska_cicd_artefact_validations.checks.{}"


class Check(ABC):
    """
    Abstract base class for defining checks.

    This class provides the structure for artefact checks, including
    methods for performing the check, returning the type, description, and
    mitigation strategy.

    Attributes:
        feature_toggle: A feature toggle associated with the check to enable
            or disable it.
    """

    feature_toggle: str = NotImplemented

    @abstractmethod
    async def check(self, artefact: Artefact) -> bool:
        """
        Executes the check on the provided artefact.

        :param artefact: The artefact on which the check will be performed.
        :return: True if the check passes, otherwise False.
        """

    @abstractmethod
    async def type(self) -> MessageType:
        """
        Returns the type of the check (e.g., failure, warning, info).

        :return: The message type representing the severity of the check
        result.
        """

    @abstractmethod
    async def description(self) -> str:
        """
        Provides a description of the check.

        :return: A string describing the check.
        """

    @abstractmethod
    async def mitigation_strategy(self) -> str:
        """
        Provides a mitigation strategy if the check fails.

        :return: A string describing the mitigation strategy.
        """


class CheckResult(BaseModel):
    """
    A class representing the result of a check execution.

    This class stores the result of a check, including whether the check
    blocks the merge request, the result status, and the runtime.

    Attributes:
        name: The name of the check.
        block_mr: Indicates if the check blocks the merge request.
        result: The result of the check execution.
        runtime: The time taken to execute the check.
    """

    name: str
    block_mr: bool
    result: bool
    runtime: float
    check_type: MessageType
    description: str
    mitigation: str

    def __init__(
        self,
        name: str,
        result: bool,
        type_: MessageType,
        runtime: float,
        description: str,
        mitigation: str,
    ) -> None:
        super().__init__(
            name=name,
            result=result,
            block_mr=(type_ == MessageType.FAILURE) and not result,
            runtime=runtime,
            check_type=type_,
            description=description,
            mitigation=mitigation,
        )


def import_check_classes() -> (
    Dict[str, List[Dict[str, Union[Type[Check], List[Type]]]]]
):
    """
    Dynamically imports check classes from the checks directory.

    This function scans the checks directory, imports the check classes, and
    returns them as a dictionary.

    :return: A dictionary containing the imported check classes.
    """

    imported_checks = {}

    check_files = os.listdir(CHECKS_FOLDER)

    for file in check_files:
        check_name = file.strip(".py")
        module_name = MODULE_PATH.format(check_name)
        module = importlib.import_module(module_name)
        module_classes = [
            class_name
            for class_name in dir(module)
            if class_name.startswith("Check") and class_name != "Check"
        ]

        for class_name in module_classes:
            check_class = getattr(module, class_name)
            check_data = {"class": check_class}
            check_data["name"] = check_name

            imported_checks.setdefault("checks", []).append(check_data)

    return imported_checks


async def perform_check(check: Check, artefact: Artefact) -> CheckResult:
    """
    Executes a check on an artefact and returns the result.

    This function runs a specific check on the provided artefact, records the
    runtime, and logs the outcome.

    :param check: The check to be performed.
    :param artefact: The artefact to be checked.
    :return: A CheckResult object containing the outcome of the check.
    """

    app_context = {"userId": artefact.name}
    if feature_toggler.is_enabled(
        f"{check.feature_toggle}-excluded",
        fallback_function=lambda feature_name, context: True,
        context=app_context,
    ):
        # If the artefact name is defined, skip the check by returning true
        logging.info(
            "Artefact %s is excluded from check %s by feature-toggle",
            artefact.name,
            check.feature_toggle,
        )
        return CheckResult(
            check.__class__.__name__, True, MessageType.INFO, 0, "", ""
        )

    if not feature_toggler.is_enabled(
        check.feature_toggle,
        fallback_function=lambda feature_name, context: True,
    ):
        logging.info("Feature toggle %s is disabled", check.feature_toggle)
        return CheckResult(
            check.__class__.__name__, True, MessageType.INFO, 0, "", ""
        )

    start = time.time()
    try:
        result = await check.check(artefact)
    except Exception as e:
        logging.error(e)
        result = False
        return CheckResult(
            check.__class__.__name__, result, MessageType.INFO, 0, "", ""
        )

    runtime = time.time() - start
    type_ = await check.type()
    description = await check.description()
    mitigation = await check.mitigation_strategy()
    name = check.__class__.__name__

    if not result:
        logging.info("Check: %s - FAILED!", name)
        logging.info("Runtime: %.2f", runtime)
        logging.info("Type: %s", type_)
        logging.info("Description: %s", description)
        logging.info("Mitigation:\n%s\n\n", mitigation)
    else:
        logging.info("Check: %s - PASSED!", name)
        logging.info("Runtime: %.2f\n", runtime)

    metrics.record_runtime(
        metrics.checks_runtime_summary,
        runtime=runtime,
        check_name=name,
    )

    return CheckResult(name, result, type_, runtime, description, mitigation)


async def run_checks(
    artefact: Artefact,
) -> List[CheckResult]:
    """
    Runs all imported checks on the given artefact.

    This function gathers all the enabled checks, executes them on the
    artefact, and separates valid results from exceptions.

    :param artefact: The artefact to be checked.
    :return: A tuple containing a list of valid check results and a
        list ofexceptions that occurred during the checks.
    """
    check_calls = []
    for check_data in checks.get("checks", []):
        check_class = check_data["class"]
        if feature_toggler.is_enabled(
            check_class.feature_toggle,
            fallback_function=lambda feature_name, context: False,
        ):
            check_call = perform_check(
                check_class(),
                artefact,
            )
            check_calls.append(check_call)

    logging.info(
        "Running checks for artefact %s:%s.\n", artefact.name, artefact.tag
    )
    check_results = await asyncio.gather(*check_calls, return_exceptions=True)

    return check_results


checks = import_check_classes()
