"""
Module for handling merge request creation tasks.

This module provides functionality to create merge requests based on the
results of artefact validations, using GitLab APIs and Celery tasks.

Constants:
    MAX_CACHE_SIZE: Maximum size of the LRU cache for caching GitLab API
        responses.
    cache: LRU cache instance used to store GitLab API responses.

Functions:
    create_merge_request: Asynchronously creates a merge request in GitLab
        based on the artefact validation results.
    create_merge_request_task: Celery task to create a merge request by
        calling the create_merge_request function.
"""

import asyncio
from typing import Optional

import aiohttp
import cachetools
from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.core.message_generator import (
    MergeRequestExecutor,
    MessageGenerator,
)
from ska_cicd_artefact_validations.mycelery import CeleryTask, app

MAX_CACHE_SIZE = 512
cache = cachetools.LRUCache(maxsize=MAX_CACHE_SIZE)


async def create_merge_request(artefact: Artefact) -> Optional[dict]:
    """
    Asynchronously creates a merge request in GitLab based on artefact
    validation results.

    This function generates messages and details from the artefact validation
    checks and creates a merge request in GitLab with the relevant information.

    :param artefact: The artefact containing validation results and metadata.
    :return: A dictionary with the merge request details if successful,
        otherwise None.
    """
    message_generator = MessageGenerator()

    artefact_status = (
        "not been promoted to production"
        if artefact.type == ArtefactType.OCI
        else "been quarantined"
    )
    await message_generator.add_pretext(
        f"The artefact {artefact.name} has {artefact_status}. "
        "Please review the table below for further information:"
    )
    await message_generator.add_posttext(
        "Please close this Merge Request and delete the associated branch "
        "once you fix these issues."
    )

    for check_result in artefact.checks_results:
        if not check_result.result:
            check_type = check_result.check_type.value
            description = check_result.description
            mitigation = check_result.mitigation
            await message_generator.add_entry(
                check_type, description, mitigation
            )

    title = f"Artefact Validation: \
            {artefact.name}:{artefact.tag} {artefact_status}."
    try:
        ci_project_id = artefact.metadata.get("CI_PROJECT_ID", "no-project-id")
        ci_job_id = artefact.metadata.get("CI_JOB_ID", "no-job-id")
        gitlab_user_id = artefact.metadata.get("GITLAB_USER_ID", "no-user-id")
    except Exception:
        logging.error(
            "Artefact %s:%s has no valid metadata."
            " Unable to create merge request.",
            artefact.name,
            artefact.tag,
        )
        return

    source_branch = f"validation-{ci_job_id}-{artefact.name}"
    assignee_ids = [gitlab_user_id] if gitlab_user_id else []
    merge_comment = await message_generator.get_message()

    try:
        async with aiohttp.ClientSession() as session:
            gitlab_api = GitLabApi(session, cache=cache)
            proj_info = await gitlab_api.get_project_settings_info(
                ci_project_id
            )
            executor = MergeRequestExecutor(gitlab_api)
            result = await executor.action(
                proj_id=ci_project_id,
                source_branch=source_branch,
                target_branch=proj_info.get("default_branch"),
                title=title,
                description_text=merge_comment,
                assignee_ids=assignee_ids,
            )
            logging.info(f"Result: {result}")
            return result
    except Exception as exc:
        logging.error("Failed to create Merge Request: %s", str(exc))


@app.task(
    base=CeleryTask,
    autoretry_for=(Exception,),
    retry_kwargs={"max_retries": 2, "countdown": 1200},
)
def create_merge_request_task(artefact: Artefact) -> Optional[dict]:
    """
    Celery task to create a merge request by calling the create_merge_request
    function.

    This task executes the asynchronous merge request creation function and
    handles retries on failure.

    :param artefact: The artefact containing validation results and metadata.
    :return: A dictionary with the merge request details if successful,
        otherwise None.
    """
    result = asyncio.run(create_merge_request(artefact))
    return result
