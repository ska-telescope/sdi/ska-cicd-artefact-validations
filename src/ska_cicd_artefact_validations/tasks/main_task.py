"""
Module for handling main validation tasks.

This module provides functionalities for validating requests related to
artifact repositories, such as Harbor and Nexus.

Functions:
    main_task: A Celery task for handling the main validation process.

"""

import asyncio

from ska_cicd_artefact_validations.celeryconfig import feature_toggler
from ska_cicd_artefact_validations.handlers.harbor.harbor_handler import (
    HarborHandler,
)
from ska_cicd_artefact_validations.handlers.nexus.nexus_handler import (
    NexusHandler,
)
from ska_cicd_artefact_validations.metrics.metrics_manager import metrics
from ska_cicd_artefact_validations.mycelery import CeleryTask, app
from ska_cicd_artefact_validations.tasks.database_task import insertdb_task
from ska_cicd_artefact_validations.tasks.merge_request_task import (
    create_merge_request_task,
)


@app.task(
    base=CeleryTask,
    autoretry_for=(Exception,),
    retry_kwargs={"max_retries": 2, "countdown": 1200},
)
def main_task(request_body, request_type):
    """
    A Celery task for handling the main validation process.

    This task processes validation requests based on the specified type
    (Harbor or Nexus), manages compliance checks, and triggers subsequent
    actions like database insertion and merge request creation.

    :param request_body: The body of the request to be validated.
    :param request_type: The type of request, which determines the handler
        (e.g., "harbor" or "nexus").
    """

    match request_type:
        case "harbor":
            artefact = asyncio.run(
                HarborHandler(feature_toggler).process_request(request_body)
            )
        case "nexus":
            artefact = asyncio.run(
                NexusHandler(feature_toggler).process_request(request_body)
            )
        case _:
            return

    insertdb_task.delay(artefact.to_json())

    if not artefact.is_compliant():
        create_merge_request_task.delay(artefact)

    metrics.increment_counter(metrics.num_uploaded_artefacts, amount=1)
    metrics.increment_counter(
        metrics.num_checks_run, amount=len(artefact.checks_results)
    )

    for check in artefact.checks_results:
        if check.result is False:
            metrics.increment_counter(
                metrics.num_checks_failed, amount=1, check_name=check.name
            )
