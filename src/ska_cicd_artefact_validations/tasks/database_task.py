"""
Module for handling MongoDB operations.

This module provides functions and tasks to interact with a MongoDB database
and manage task execution using Celery.

Functions:
    get_mongodb: Connects to MongoDB and returns the specified collection.
    insertdb_task: Celery task to insert a document into the collection.
"""

import os

from pymongo import MongoClient

from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.metrics.metrics_manager import metrics
from ska_cicd_artefact_validations.mycelery import CeleryTask, app


def get_mongodb():
    """
    Connects to MongoDB and returns the specified collection.

    This function establishes a connection to the MongoDB server using the
    environment variables for the URL, database name, and collection name.

    :return: The MongoDB collection object.
    """
    client = MongoClient(os.getenv("MONGO_URL"))
    db = client[os.getenv("MONGO_DB")]
    collection = db[os.getenv("MONGO_COL")]
    return collection


@app.task(
    base=CeleryTask,
    autoretry_for=(Exception,),
    retry_kwargs={"max_retries": 2, "countdown": 1200},
)
def insertdb_task(document: dict):
    """
    Celery task to insert a document into the MongoDB collection.

    This task connects to MongoDB and attempts to insert the provided document
    into the collection. It retries on failure and logs any exceptions.

    :param document: The document to be inserted into the MongoDB collection.
    :return: True if the insertion is successful.
    :raises Exception: If there are problems inserting the document into the
        database.
    """
    try:
        collection = get_mongodb()
        collection.insert_one(document)
    except Exception as ex:
        logging.error("Insert to MongoDB exception : %s", ex)
        metrics.increment_counter(
            metrics.num_exceptions, amount=1, function_name="insertdb_task"
        )
        raise Exception("Problems getting metadata to db") from ex

    return True
