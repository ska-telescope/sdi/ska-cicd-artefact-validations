"""
Module for instantiating a Celery application.

This module provides functionality to create a Celery app with configurations
from the `celeryconfig` module. It also initializes a task logging for the
Celery app.

Attributes:
    name (str): The name of the Celery application.
    app (Celery): The instantiated Celery application.
"""

import os

from celery import Celery, Task

from ska_cicd_artefact_validations import celeryconfig
from ska_cicd_artefact_validations.core.logging import logging

name = os.getenv("CELERY_APP_NAME", default="Artefact_Validations_Celery")
app = Celery(name, config_source=celeryconfig)
app.conf.task_serializer = "pickle"
app.conf.result_serializer = "pickle"
app.conf.accept_content = [
    "application/json",
    "application/x-python-serialize",
]
app.conf.update(
    worker_log_format="[%(asctime)s] %(levelname)s: %(message)s",
    task_log_format="[%(asctime)s] %(levelname)s [%(task_name)s]: %(message)s",
)


def log_task_on_start(task_id, args, kwargs):
    """
    Logs information when a task starts.

    This function logs the task ID, arguments, and keyword arguments when the
    task begins execution.

    :param task_id: Unique ID of the task that is starting.
    :param args: Arguments passed to the task.
    :param kwargs: Keyword arguments passed to the task.
    """
    request_info = {"task_id": task_id, "args": args, "kwargs": kwargs}
    logging.debug("Task Starting: %s", request_info)


def log_task_failure(exc, task_id, args, kwargs, einfo):
    """
    Error handler implementation.

    This is run by the worker when the task fails.

    :param exc: The exception raised by the task.
    :param task_id: Unique id of the failed task.
    :param args: Original arguments for the task that failed.
    :param kwargs: Original keyword arguments for the task that failed.
    :param einfo: Exception information.
    """
    info = {
        "status": "Failed",
        "exception": str(exc),
        "task_id": task_id,
        "task_args": args,
        "task_kwargs": kwargs,
        "exception_info": str(einfo),
    }
    logging.debug("Task Failed: %s", info)


def log_task_retry(exc, task_id, args, kwargs, einfo):
    """
    Retry handler implementation.

    This is run by the worker when the task is to be retried.

    Args:
        exc (Exception) - The exception raised by the task.
        task_id (str) - Unique id of the failed task.
        args (Tuple) - Original arguments for the task that failed.
        kwargs (Dict) - Original keyword arguments for the task that failed.
        einfo (ExceptionInfo) - Exception information.
    """
    info = {
        "status": "Retrying",
        "exception": str(exc),
        "task_id": task_id,
        "task_args": args,
        "task_kwargs": kwargs,
        "exception_info": str(einfo),
    }
    logging.debug("Task Retried: %s", info)


def log_task_success(retval, task_id, args, kwargs):
    """
    Success handler implementation.

    Run by the worker if the task executes successfully.

    Args:
        retval (Any) - The return value of the task.
        task_id (str) - Unique id of the executed task.
        args (Tuple) - Original arguments for the executed task.
        kwargs (Dict) - Original keyword arguments for the executed task.
    """
    info = {
        "return_value": retval,
        "task_id": task_id,
        "task_args": args,
        "task_kwargs": kwargs,
    }
    logging.debug("Task Succeeded: %s", info)


# pylint: disable=abstract-method
class CeleryTask(Task):
    """
    Custom Celery task class with additional logging for task lifecycle events.

    This class extends the base Celery Task class to include custom logging
    methods for task start, failure, retry, and success events.
    """

    def __call__(self, *args, **kwargs):
        """
        Calls the task and triggers the on_start event.

        This method is called when the task is executed, and it logs the
        task start event before proceeding with the task execution.

        :param args: Arguments passed to the task.
        :param kwargs: Keyword arguments passed to the task.
        """
        self.on_start(self.request.id, args, kwargs)
        return super().__call__(*args, **kwargs)

    def on_start(self, task_id, args, kwargs):
        """
        Logs information when the task starts.

        This method is triggered before the task execution begins, logging the
        task ID, arguments, and keyword arguments.

        :param task_id: Unique ID of the task that is starting.
        :param args: Arguments passed to the task.
        :param kwargs: Keyword arguments passed to the task.
        """
        log_task_on_start(task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Logs information when the task fails.

        This method is called when the task execution fails, logging details
        about the exception, task ID, arguments, and exception information.

        :param exc: The exception raised by the task.
        :param task_id: Unique ID of the failed task.
        :param args: Original arguments for the task that failed.
        :param kwargs: Original keyword arguments for the task that failed.
        :param einfo: Exception information.
        """
        log_task_failure(exc, task_id, args, kwargs, einfo)

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        """
        Logs information when the task is retried.

        This method is called when the task is set to be retried, logging
        details about the exception, task ID, arguments, and exception
        information.

        :param exc: The exception raised by the task.
        :param task_id: Unique ID of the task to be retried.
        :param args: Original arguments for the task that failed.
        :param kwargs: Original keyword arguments for the task that failed.
        :param einfo: Exception information.
        """
        log_task_retry(exc, task_id, args, kwargs, einfo)

    def on_success(self, retval, task_id, args, kwargs):
        """
        Logs information when the task succeeds.

        This method is called when the task completes successfully, logging the
        return value, task ID, arguments, and keyword arguments.

        :param retval: The return value of the task.
        :param task_id: Unique ID of the executed task.
        :param args: Original arguments for the executed task.
        :param kwargs: Original keyword arguments for the executed task.
        """
        log_task_success(retval, task_id, args, kwargs)
