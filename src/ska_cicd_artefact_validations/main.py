"""
Module for running the Celery worker.

This module imports the Celery application instance and runs the worker when
executed as the main program.
"""

from ska_cicd_artefact_validations.mycelery import app

if __name__ == "__main__":
    app.worker_main()
