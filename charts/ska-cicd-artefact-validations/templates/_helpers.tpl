{{/*
Expand the name of the chart.
*/}}
{{- define "ska-cicd-artefact-validations.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ska-cicd-artefact-validations.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ska-cicd-artefact-validations.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ska-cicd-artefact-validations.labels" -}}
helm.sh/chart: {{ include "ska-cicd-artefact-validations.chart" . }}
{{ include "ska-cicd-artefact-validations.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ska-cicd-artefact-validations.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ska-cicd-artefact-validations.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ska-cicd-artefact-validations.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ska-cicd-artefact-validations.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Merge configuration values from multiple YAML blocks into one.
*/}}
{{- define "ska-cicd-artefact-validations.config" -}}
{{- template "ska-cicd-artefact-validations.merge" (list
  (toYaml .Values.config)
) -}}
{{- end -}}

{{/*
Get the parent directory from the configuration path.
This template removes the last part of a path, leaving only the parent directory.
*/}}
{{- define "ska-cicd-artefact-validations.configPath" -}}
{{- regexReplaceAll "/[^/]+$" .Values.configPath "" -}}
{{- end -}}

{{- define "ska-cicd-artefact-validations.namespace" -}}
{{ .Release.Namespace }}
{{- end -}}

{{- define "ska-cicd-artefact-validations.merge" -}}
{{- $merged := dict -}}
{{- range . -}}
  {{- $merged = mergeOverwrite $merged (fromYaml .) -}}
{{- end -}}
{{- with $merged -}}
  {{- toYaml $merged -}}
{{- end -}}
{{- end -}}
