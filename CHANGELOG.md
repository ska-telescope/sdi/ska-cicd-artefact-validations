CHANGELOG
=========

## 1.1.3

- Configure the Unleash client to use the Unleash Proxy for feature toggle evaluation instead of directly communicating with the Unleash Server.
- Add required configuration for the Unleash Proxy, including:
  - UNLEASH_PROXY_URL
  - UNLEASH_PROXY_SDK_TOKEN
- Replaced occurrences of variables pointing to the Unleash Server, such as:
  - UNLEASH_API_URL → Updated to UNLEASH_PROXY_URL
  - UNLEASH_API_TOKEN → Updated to UNLEASH_PROXY_SDK_TOKEN

### **Release Notes Information**
- This `CHANGELOG.md` file was added starting with version 1.1.3. You can find the previous release's notes in the [Release Notes](https://gitlab.com/ska-telescope/sdi/ska-cicd-artefact-validations/-/releases/1.1.2) for version 1.1.2.