import unittest
from unittest.mock import MagicMock, patch

from prometheus_client import CollectorRegistry, Counter, Summary

from ska_cicd_artefact_validations.core.utils import check_metrics_toggle
from ska_cicd_artefact_validations.metrics.metrics_manager import (
    MetricsManager,
)


class TestCheckMetricsToggle(unittest.TestCase):
    @patch("ska_cicd_artefact_validations.core.utils.feature_toggler")
    def test_metrics_toggle_disabled(self, mock_toggler):
        mock_toggler.is_enabled.return_value = False
        mock_function = MagicMock()

        decorator = check_metrics_toggle(mock_function)
        decorator()
        mock_function.assert_not_called()

    @patch("ska_cicd_artefact_validations.core.utils.feature_toggler")
    def test_metrics_toggle_enabled(self, mock_toggler):
        mock_toggler.is_enabled.return_value = True
        mock_function = MagicMock()

        decorator = check_metrics_toggle(mock_function)
        decorator()
        mock_function.assert_called_once()


class TestMetricsManager(unittest.TestCase):
    def test_singleton_pattern(self):
        manager1 = MetricsManager()
        manager2 = MetricsManager()
        self.assertIs(manager1, manager2)

    def test_metric_initialization(self):
        manager = MetricsManager()
        # Check if metrics are set up correctly
        self.assertTrue(
            hasattr(manager, "checks_runtime_summary"),
            "checks_runtime_summary should be initialized.",
        )
        self.assertTrue(
            hasattr(manager, "num_checks_run"),
            "num_checks_run should be initialized.",
        )
        self.assertTrue(
            hasattr(manager, "num_checks_failed"),
            "num_checks_failed should be initialized.",
        )
        self.assertTrue(
            hasattr(manager, "num_uploaded_artefacts"),
            "num_uploaded_artefacts should be initialized.",
        )
        self.assertTrue(
            hasattr(manager, "num_exceptions"),
            "num_exceptions should be initialized.",
        )

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=True,
    )
    def test_increment_counter_enabled(self, mock_is_enabled):
        mock_counter = MagicMock()
        manager = MetricsManager()
        manager.increment_counter(
            mock_counter, label1="value1", label2="value2"
        )
        # Check if the counter's inc method was called
        mock_counter.labels.assert_called_once_with(
            label1="value1", label2="value2"
        )
        mock_counter.labels.return_value.inc.assert_called_once()
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=False,
    )
    def test_increment_counter_disabled(self, mock_is_enabled):
        mock_counter = MagicMock()
        manager = MetricsManager()
        manager.increment_counter(
            mock_counter, label1="value1", label2="value2"
        )
        # Check if the counter's inc method was called
        mock_counter.labels.assert_not_called()
        mock_counter.labels.return_value.inc.assert_not_called()
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=True,
    )
    def test_record_runtime_enabled(self, mock_is_enabled):
        mock_summary = MagicMock()
        manager = MetricsManager()
        manager.record_runtime(
            mock_summary, 5.5, label1="value1", label2="value2"
        )
        # Check if the counter's inc method was called
        mock_summary.labels.assert_called_once_with(
            label1="value1", label2="value2"
        )
        mock_summary.labels.return_value.observe.assert_called_once_with(5.5)
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=False,
    )
    def test_record_runtime_disabled(self, mock_is_enabled):
        mock_summary = MagicMock()
        manager = MetricsManager()
        manager.record_runtime(
            mock_summary, 5.5, label1="value1", label2="value2"
        )
        # Check if the counter's inc method was called
        mock_summary.labels.assert_not_called()
        mock_summary.labels.return_value.inc.assert_not_called()
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=True,
    )
    def test_counter_inc_wrapped_enabled(self, mock_is_enabled):
        new_registry = CollectorRegistry()
        counter = Counter("metric_name", "Test Counter", registry=new_registry)
        manager = MetricsManager()
        manager.increment_counter(counter, amount=1)
        self.assertEqual(counter._value.get(), 1)
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=False,
    )
    def test_counter_inc_wrapped_disabled(self, mock_is_enabled):
        new_registry = CollectorRegistry()
        counter = Counter("metric_name", "Test Counter", registry=new_registry)
        manager = MetricsManager()
        manager.increment_counter(counter, amount=1)
        self.assertEqual(counter._value.get(), 0)
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=True,
    )
    def test_summary_observe_wrapped_enabled(self, mock_is_enabled):
        new_registry = CollectorRegistry()
        summary = Summary("metric_name", "Test Summary", registry=new_registry)
        manager = MetricsManager()
        manager.record_runtime(summary, runtime=10)
        self.assertEqual(summary._count.get(), 1)
        self.assertEqual(summary._sum.get(), 10)
        mock_is_enabled.assert_called()

    @patch(
        "ska_cicd_artefact_validations.core.utils.feature_toggler.is_enabled",
        return_value=False,
    )
    def test_summary_observe_wrapped_disabled(self, mock_is_enabled):
        new_registry = CollectorRegistry()
        summary = Summary("metric_name", "Test Summary", registry=new_registry)
        manager = MetricsManager()
        manager.record_runtime(summary, runtime=10)
        self.assertEqual(summary._count.get(), 0)
        self.assertEqual(summary._sum.get(), 0)
        mock_is_enabled.assert_called()

    def test_get_metrics(self):
        manager = MetricsManager()
        # Mock the generate_latest to return specific bytes
        with patch(
            "ska_cicd_artefact_validations.metrics.metrics_manager.generate_latest",  # pylint: disable=line-too-long # noqa: E501
            return_value=b"Metrics data",
        ) as mock_generate:
            metrics_output = manager.get_metrics()
            mock_generate.assert_called_once_with(manager.registry)
            self.assertEqual(
                metrics_output,
                b"Metrics data",
                "get_metrics should return the correct metrics data.",
            )
