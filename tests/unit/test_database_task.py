import unittest
from unittest.mock import MagicMock, patch

from ska_cicd_artefact_validations.tasks.database_task import (
    get_mongodb,
    insertdb_task,
)


@patch("ska_cicd_artefact_validations.tasks.database_task.MongoClient")
@patch("ska_cicd_artefact_validations.tasks.database_task.os.getenv")
def test_get_mongodb(mock_getenv, mock_mongo_client):
    # Mock environment variables
    mock_getenv.side_effect = lambda var: {
        "MONGO_URL": "mongodb://localhost:27017",
        "MONGO_DB": "test_db",
        "MONGO_COL": "test_collection",
    }[var]

    # Mock the MongoClient and its return values
    mock_db = MagicMock()
    mock_collection = MagicMock()
    mock_mongo_client.return_value.__getitem__.return_value = mock_db
    mock_db.__getitem__.return_value = mock_collection

    collection = get_mongodb()

    # Assert the correct collection is returned
    assert collection == mock_collection

    # Assert MongoClient is called with the correct URL
    mock_mongo_client.assert_called_with("mongodb://localhost:27017")
    mock_mongo_client.return_value.__getitem__.assert_called_with("test_db")
    mock_db.__getitem__.assert_called_with("test_collection")


@patch("ska_cicd_artefact_validations.tasks.database_task.get_mongodb")
def test_insertdb_task_success(mock_get_mongodb):
    # Mock the MongoDB collection
    mock_collection = MagicMock()
    mock_get_mongodb.return_value = mock_collection

    document = {"key": "value"}

    # Call the task
    result = insertdb_task(document)

    # Assert the document is inserted
    mock_collection.insert_one.assert_called_with(document)

    # Assert the task returns True
    assert result is True


@patch("ska_cicd_artefact_validations.tasks.database_task.get_mongodb")
def test_insertdb_task_exception(mock_get_mongodb):
    # Mock the MongoDB collection to raise an exception
    mock_collection = MagicMock()
    mock_collection.insert_one.side_effect = Exception("Insertion error")
    mock_get_mongodb.return_value = mock_collection

    document = {"key": "value"}

    # Call the task and assert it raises an exception
    with unittest.TestCase().assertRaises(Exception) as context:
        insertdb_task(document)

    # Assert the exception message is correct
    assert str(context.exception) == "Problems getting metadata to db"
