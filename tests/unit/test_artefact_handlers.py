from unittest.mock import patch

import pytest

from ska_cicd_artefact_validations.core.artefact import Artefact
from ska_cicd_artefact_validations.handlers.nexus.artefact_handlers import (
    ConanHandler,
)


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.getenv"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_add_remote_success(
    mock_nexus_api, mock_session, mock_getenv, mock_system
):
    # Arrange
    mock_system.return_value = 0
    mock_getenv.return_value = "http://nexus.local"
    handler = ConanHandler(session=mock_session)

    # Act
    await handler._ConanHandler__add_remote("test-repository")

    # Assert
    mock_system.assert_called_once_with(
        "conan remote add test-repository http://nexus.local/repository/test-repository --force"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_getenv.assert_called_with("NEXUS_URL")


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.getenv"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_add_remote_failure(
    mock_nexus_api, mock_session, mock_getenv, mock_system
):
    # Arrange
    mock_system.return_value = 1
    mock_getenv.return_value = "http://nexus.local"
    handler = ConanHandler(session=mock_session)

    # Act & Assert
    with pytest.raises(
        Exception, match="Couldn't add the conan remote repository."
    ):
        await handler._ConanHandler__add_remote("test-repository")

    # Assert
    mock_system.assert_called_once_with(
        "conan remote add test-repository http://nexus.local/repository/test-repository --force"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_getenv.assert_called_with("NEXUS_URL")


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.getenv"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ConanHandler._ConanHandler__add_remote"  # pylint: disable=line-too-long # noqa: E501
)
async def test_authenticate_success(
    mock_add_remote, mock_nexus_api, mock_session, mock_getenv, mock_system
):
    # Arrange
    mock_system.return_value = 0
    mock_getenv.side_effect = ["api_user", "api_pwd"]
    handler = ConanHandler(session=mock_session)

    # Act
    await handler._ConanHandler__authenticate("test-repository")

    # Assert
    mock_system.assert_called_once_with(
        "conan user api_user -p api_pwd -r test-repository"
    )

    mock_getenv.assert_any_call("NEXUS_API_USERNAME")
    mock_getenv.assert_any_call("NEXUS_API_PASSWORD")


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.getenv"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ConanHandler._ConanHandler__add_remote"  # pylint: disable=line-too-long # noqa: E501
)
async def test_authenticate_failure(
    mock_add_remote, mock_nexus_api, mock_session, mock_getenv, mock_system
):
    # Arrange
    mock_system.return_value = 1
    mock_getenv.side_effect = ["api_user", "api_pwd"]
    handler = ConanHandler(session=mock_session)

    # Act & Assert
    with pytest.raises(
        Exception, match="Couldn't authenticate with the conan repository."
    ):
        await handler._ConanHandler__authenticate("test-repository")

    # Assert
    mock_system.assert_called_once_with(
        "conan user api_user -p api_pwd -r test-repository"
    )

    mock_getenv.assert_any_call("NEXUS_API_USERNAME")
    mock_getenv.assert_any_call("NEXUS_API_PASSWORD")


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ConanHandler._ConanHandler__authenticate"  # pylint: disable=line-too-long # noqa: E501
)
async def test_upload_success(
    mock_authenticate, mock_nexus_api, mock_session, mock_system
):
    # Arrange
    artefact = Artefact(
        name="artefact-name",
        tag="artefact-tag",
        group="artefact-group",
        channel="artefact-channel",
        project="artefact-project",
    )
    mock_system.return_value = 0
    handler = ConanHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, "test-repository")

    # Assert
    assert result is True
    mock_system.assert_any_call(
        "conan install artefact-name/artefact-tag@artefact-group/artefact-channel -r artefact-project"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_system.assert_any_call(
        "conan upload artefact-name/artefact-tag@artefact-group/artefact-channel -r test-repository --all"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_system.assert_any_call(
        "conan remove artefact-name/artefact-tag@artefact-group/artefact-channel --force"  # pylint: disable=line-too-long # noqa: E501
    )


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ConanHandler._ConanHandler__authenticate"  # pylint: disable=line-too-long # noqa: E501
)
async def test_upload_install_fail(
    mock_authenticate, mock_nexus_api, mock_session, mock_system
):
    # Arrange
    artefact = Artefact(
        name="artefact-name",
        tag="artefact-tag",
        group="artefact-group",
        channel="artefact-channel",
        project="artefact-project",
    )

    mock_system.side_effect = [1, 0, 0]
    handler = ConanHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, "test-repository")

    # Assert
    assert result is False
    mock_system.assert_any_call(
        "conan install artefact-name/artefact-tag@artefact-group/artefact-channel -r artefact-project"  # pylint: disable=line-too-long # noqa: E501
    )


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ConanHandler._ConanHandler__authenticate"  # pylint: disable=line-too-long # noqa: E501
)
async def test_upload_upload_fail(
    mock_authenticate, mock_nexus_api, mock_session, mock_system
):
    # Arrange
    artefact = Artefact(
        name="artefact-name",
        tag="artefact-tag",
        group="artefact-group",
        channel="artefact-channel",
        project="artefact-project",
    )

    mock_system.side_effect = [0, 1, 0]
    handler = ConanHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, "test-repository")

    # Assert
    assert result is False
    mock_system.assert_any_call(
        "conan install artefact-name/artefact-tag@artefact-group/artefact-channel -r artefact-project"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_system.assert_any_call(
        "conan upload artefact-name/artefact-tag@artefact-group/artefact-channel -r test-repository --all"  # pylint: disable=line-too-long # noqa: E501
    )


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.os.system"
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ConanHandler._ConanHandler__authenticate"  # pylint: disable=line-too-long # noqa: E501
)
async def test_upload_remove_fail(
    mock_authenticate, mock_nexus_api, mock_session, mock_system
):
    # Arrange
    artefact = Artefact(
        name="artefact-name",
        tag="artefact-tag",
        group="artefact-group",
        channel="artefact-channel",
        project="artefact-project",
    )

    mock_system.side_effect = [0, 0, 1]
    handler = ConanHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, "test-repository")

    # Assert
    assert result is True
    mock_system.assert_any_call(
        "conan install artefact-name/artefact-tag@artefact-group/artefact-channel -r artefact-project"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_system.assert_any_call(
        "conan upload artefact-name/artefact-tag@artefact-group/artefact-channel -r test-repository --all"  # pylint: disable=line-too-long # noqa: E501
    )
    mock_system.assert_any_call(
        "conan remove artefact-name/artefact-tag@artefact-group/artefact-channel --force"  # pylint: disable=line-too-long # noqa: E501
    )
