from unittest.mock import AsyncMock, MagicMock, mock_open, patch

import pytest

from ska_cicd_artefact_validations.handlers.nexus.artefact_handlers import (
    AssetDownloader,
)


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch("aiohttp.ClientSession")
@patch(
    "builtins.open", new_callable=mock_open
)  # Mock file open to avoid actual file operations
@patch("os.makedirs")  # Mock os.makedirs to avoid creating actual directories
@patch(
    "os.path.exists", return_value=False
)  # Mock os.path.exists to simulate directory does not exist
async def test_download_assets(
    mock_exists, mock_makedirs, mock_open_file, mock_session, mock_nexus_api
):
    # Arrange
    component_id = "test-component-id"
    asset_url = "http://example.com/test-asset.tar.gz"
    asset_name = "test-asset.tar.gz"
    component_info = {"assets": [{"downloadUrl": asset_url}]}

    mock_nexus_api.get_component_info = AsyncMock(return_value=component_info)

    # Mock aiohttp session get response
    mock_resp = MagicMock()
    mock_resp.content.read = AsyncMock(
        side_effect=[b"chunk1", b"chunk2", b""]
    )  # Mocking chunked reading
    mock_session.get = AsyncMock(return_value=mock_resp)

    # Mock file handle and its attributes
    mock_open_file.return_value.name = (
        f"{AssetDownloader.ASSETS_FOLDER}/{asset_name}"
    )

    # Create AssetDownloader instance
    downloader = AssetDownloader(api=mock_nexus_api, session=mock_session)

    # Act
    result = await downloader.download(component_id)

    # Assert
    assert len(result) == 1  # Ensure only one file was downloaded
    assert (
        result[0] == f"{AssetDownloader.ASSETS_FOLDER}/{asset_name}"
    )  # Ensure the filepath is returned
    mock_nexus_api.get_component_info.assert_awaited_once_with(
        component_id
    )  # Ensure API was called once with component ID
    mock_session.get.assert_called_once_with(
        asset_url
    )  # Ensure session get was called once with the asset URL

    # Ensure the response content was read
    mock_resp.content.read.assert_called()

    mock_open_file.assert_called_once_with(
        f"{AssetDownloader.ASSETS_FOLDER}/{asset_name}", "wb"
    )  # Ensure file was opened for writing

    # Check that chunks were written to the file
    handle = mock_open_file()
    handle.write.assert_any_call(b"chunk1")
    handle.write.assert_any_call(b"chunk2")


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch("aiohttp.ClientSession")
async def test_download_no_assets(mock_session, mock_nexus_api):
    # Arrange
    component_id = "test-component-id"
    component_info = {}

    mock_nexus_api.get_component_info = AsyncMock(return_value=component_info)

    # Mock aiohttp session get response
    mock_resp = MagicMock()
    mock_resp.content.read = AsyncMock(
        side_effect=[b"chunk1", b"chunk2", b""]
    )  # Mocking chunked reading
    mock_session.get = AsyncMock(return_value=None)

    # Create AssetDownloader instance
    downloader = AssetDownloader(api=mock_nexus_api, session=mock_session)

    # Assert KeyError is raised
    with pytest.raises(KeyError, match="assets"):
        # Act
        _ = await downloader.download(component_id)


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
@patch("aiohttp.ClientSession")
@patch(
    "builtins.open", new_callable=mock_open
)  # Mock file open to avoid actual file operations
@patch("os.makedirs")  # Mock os.makedirs to avoid creating actual directories
@patch(
    "os.path.exists", return_value=False
)  # Mock os.path.exists to simulate directory does not exist
async def test_download_assets_no_download_url(
    mock_exists, mock_makedirs, mock_open_file, mock_session, mock_nexus_api
):
    # Arrange
    component_id = "test-component-id"
    component_info = {"assets": [{}]}

    mock_nexus_api.get_component_info = AsyncMock(return_value=component_info)

    # Mock aiohttp session get response
    mock_resp = MagicMock()
    mock_resp.content.read = AsyncMock(
        side_effect=[b"chunk1", b"chunk2", b""]
    )  # Mocking chunked reading
    mock_session.get = AsyncMock(return_value=None)

    # Create AssetDownloader instance
    downloader = AssetDownloader(api=mock_nexus_api, session=mock_session)

    # Assert KeyError is raised
    with pytest.raises(KeyError, match="downloadUrl"):
        # Act
        _ = await downloader.download(component_id)
