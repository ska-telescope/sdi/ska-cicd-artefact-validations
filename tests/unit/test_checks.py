import asyncio
import importlib
import json
import os
import tarfile
from unittest.mock import AsyncMock, MagicMock, patch

import pytest
from pytest_bdd import given, parsers, scenarios, then

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.check import (
    Check,
    MessageType,
    import_check_classes,
    perform_check,
)

feature_toggler_mock = MagicMock()

# Scenario path
scenarios("./test_checks.feature")


def format_value(value):
    try:
        if value != "None":
            value = json.loads(value)
            if isinstance(value, bool):
                return value

            if isinstance(value, int) or isinstance(value, float):
                value = str(value)

        else:
            value = None

    except json.JSONDecodeError:
        pass
    return value


@given(parsers.parse("an Artefact with {attribute} attribute from {file}"))
def create_artefact_with_data_from_file(attribute, file):
    with open(file, "r", encoding="utf-8") as f:
        data = json.loads(f.read())

    field = {attribute: data}
    pytest.artefact = Artefact(**field)


@given(parsers.parse("an Artefact with {attribute} attribute set to {value}"))
def create_artefact_with_data(attribute, value):
    value = format_value(value)
    field = {attribute: value}
    pytest.artefact = Artefact(**field)


@given(parsers.parse("{attribute} attribute set to {value}"))
def append_attribute_value(attribute, value):
    value = format_value(value)
    setattr(pytest.artefact, attribute, value)


@given(parsers.parse("type attribute set to {value}"))
def append_type_value(value):
    value = format_value(value)
    value = ArtefactType(value)
    pytest.artefact.type = value


@given(parsers.parse("an Artefact with {attribute} attribute set to <value>"))
def create_artefact_with_data_examples(attribute, value):
    create_artefact_with_data(attribute, value)


@given(parsers.parse("create files for artefact assets"))
def create_files_from_assets():
    for asset in pytest.artefact.assets:
        with tarfile.open(asset, "w") as tar:
            tarinfo = tarfile.TarInfo(name="file.txt")
            tarinfo.size = 0
            tar.addfile(tarinfo)


@then(parsers.parse("delete files"))
def delete_files_from_assets():
    for asset in pytest.artefact.assets:
        os.remove(asset)


@then(parsers.parse("{check_name} should return {result}"))
def check_check(check_name, result):
    result = format_value(result)
    mod = importlib.import_module(
        f"ska_cicd_artefact_validations.checks.{check_name}"
    )
    check_class = getattr(mod, check_name.title().replace("_", ""))
    check = check_class()

    assert result == asyncio.run(check.check(pytest.artefact))


@then(parsers.parse("{check_name} should return <result>"))
def check_check_examples(check_name, result):
    check_check(check_name, result)


def setup_check_module():
    artefact = Artefact(name="test_artefact")
    check_instance = AsyncMock(spec=Check)
    check_instance.feature_toggle = "test_toggle"
    check_instance.type.return_value = MessageType.INFO
    check_instance.description.return_value = "Test description"
    check_instance.mitigation_strategy.return_value = "Test mitigation"
    check_instance.check.return_value = True
    return artefact, check_instance


@patch(
    "src.ska_cicd_artefact_validations.core.check.feature_toggler.is_enabled",
    side_effect=[False, True],
)
@patch(
    "src.ska_cicd_artefact_validations.core.check.metrics.record_runtime",
    return_value=None,
)
def test_perform_check_passed(mock_feature_toggler, mock_metrics):
    artefact, check_instance = setup_check_module()

    result = asyncio.run(perform_check(check_instance, artefact))

    assert result.result is True


@patch(
    "src.ska_cicd_artefact_validations.core.check.feature_toggler.is_enabled",
    side_effect=[False, True],
)
@patch(
    "src.ska_cicd_artefact_validations.core.check.metrics.record_runtime",
    return_value=None,
)
def test_perform_check_failed(mock_feature_toggler, mock_metrics):
    artefact, check_instance = setup_check_module()
    check_instance.check.return_value = False

    result = asyncio.run(perform_check(check_instance, artefact))

    assert result.result is False


@patch("src.ska_cicd_artefact_validations.core.check.os.listdir")
@patch("src.ska_cicd_artefact_validations.core.check.importlib.import_module")
def test_import_check_classes(mock_import_module, mock_listdir):
    mock_listdir.return_value = ["test_check.py"]
    mock_module = MagicMock()
    mock_import_module.return_value = mock_module
    mock_module.CheckTest = Check

    imported_checks = import_check_classes()

    assert "checks" in imported_checks
    assert len(imported_checks["checks"]) == 1
    assert imported_checks["checks"][0]["name"] == "test_check"
    assert imported_checks["checks"][0]["class"] == mock_module.CheckTest
