from unittest.mock import AsyncMock, MagicMock, patch

import pytest

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.check import CheckResult
from ska_cicd_artefact_validations.handlers.nexus.nexus_handler import (
    NexusHandler,
)


# Fixture for NexusHandler
@pytest.fixture(name="nexus_handler")
def get_nexus_handler():
    feature_toggler_mock = MagicMock()
    feature_toggler_mock.is_enabled.return_value = True
    with patch(
        "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi",  # pylint: disable=line-too-long # noqa: E501
        autospec=True,
    ) as mock_nexus_api:
        handler = NexusHandler(feature_toggler_mock)
        handler.api = mock_nexus_api
    yield handler


def test_create_artefact_from_request_success(nexus_handler):
    # Arrange
    request_body = {
        "repositoryName": "test-repo",
        "component": {
            "name": "test-component",
            "format": "pypi",
            "componentId": "1234",
            "version": "1.0.0",
            "group": "test-group",
            "channel": "test-channel",
        },
    }

    # Act
    artefact = nexus_handler._create_artefact_from_request(request_body)

    # Assert
    assert artefact.project == "test-repo"
    assert artefact.name == "test-component"
    assert artefact.type == ArtefactType.PYPI
    assert artefact.id == "1234"
    assert artefact.tag == "1.0.0"
    assert artefact.group == "test-group"
    assert artefact.channel == "test-channel"


def test_create_artefact_from_request_missing_fields(nexus_handler):
    # Arrange
    request_body = {
        "repositoryName": "test-repo",
        "component": {
            "name": "test-component",
            "format": "pypi",
            "componentId": "1234",
            "version": "1.0.0",
        },
    }

    # Act
    artefact = nexus_handler._create_artefact_from_request(request_body)

    # Assert
    assert artefact.project == "test-repo"
    assert artefact.name == "test-component"
    assert artefact.type == ArtefactType.PYPI
    assert artefact.id == "1234"
    assert artefact.tag == "1.0.0"
    assert artefact.group is None
    assert artefact.channel is None


def test_create_artefact_from_request_no_version(nexus_handler):
    # Arrange
    request_body = {
        "repositoryName": "test-repo",
        "component": {
            "name": "test-component",
            "format": "pypi",
            "componentId": "1234",
            "group": "test-group",
            "channel": "test-channel",
        },
    }

    # Act
    artefact = nexus_handler._create_artefact_from_request(request_body)

    # Assert
    assert artefact.project == "test-repo"
    assert artefact.name == "test-component"
    assert artefact.type == ArtefactType.PYPI
    assert artefact.id == "1234"
    assert artefact.tag == "no-version"
    assert artefact.group == "test-group"
    assert artefact.channel == "test-channel"


@pytest.mark.parametrize(
    "request_body, expected_out",
    [
        (
            {
                "repositoryName": "test-repo",
                "component": {
                    "name": "raw-component-1.2.3.tar.gz",
                    "format": "raw",
                    "componentId": "5678",
                },
            },
            {
                "project": "test-repo",
                "name": "raw-component",
                "type": ArtefactType.RAW,
                "id": "5678",
                "tag": "1.2.3",
            },
        ),
        (
            {
                "repositoryName": "test-repo",
                "component": {
                    "name": "raw-component-1.2.3-rc1.tar.gz",
                    "format": "raw",
                    "componentId": "5678",
                },
            },
            {
                "project": "test-repo",
                "name": "raw-component",
                "type": ArtefactType.RAW,
                "id": "5678",
                "tag": "1.2.3-rc1",
            },
        ),
    ],
)
def test_create_artefact_from_request_raw_type(
    request_body, expected_out, nexus_handler
):
    """
    Test the creation of an Artefact object of type RAW,
    which requires special name parsing.
    """
    # Act
    artefact = nexus_handler._create_artefact_from_request(request_body)

    # Assert
    assert artefact.project == expected_out["project"]
    assert artefact.name == expected_out["name"]
    assert artefact.type == expected_out["type"]
    assert artefact.id == expected_out["id"]
    assert artefact.tag == expected_out["tag"]


@pytest.mark.asyncio
async def test_nexus_handler_process_request_non_compliant(nexus_handler):
    request_body = {
        "repositoryName": "test-repo",
        "component": {
            "name": "test-component",
            "format": "pypi",
            "componentId": "1234",
            "version": "1.0.0",
            "group": "test-group",
            "channel": "test-channel",
        },
    }
    quarantine_repo = "test-quarantine"

    with patch(
        "ska_cicd_artefact_validations.handlers.nexus.nexus_handler.NexusArtefactHandler",  # pylint: disable=line-too-long # noqa: E501
        autospec=True,
    ) as mock_nexus_artefact_handler, patch(
        "ska_cicd_artefact_validations.handlers.nexus.nexus_handler.run_checks",  # pylint: disable=line-too-long # noqa: E501
        new_callable=AsyncMock,
    ) as run_checks_mock, patch(
        "ska_cicd_artefact_validations.handlers.nexus.nexus_handler.NexusHandler._create_artefact_from_request",  # pylint: disable=line-too-long # noqa: E501
        new_callable=MagicMock,
    ) as create_artefact_mock:

        created_artefact = Artefact(
            project="test-repo",
            name="test-component",
            type=ArtefactType.PYPI,
            id="1234",
            tag="1.0.0",
            group="test-group",
            channel="test-channel",
        )
        create_artefact_mock.return_value = created_artefact

        mock_nexus_artefact_handler_instance = (
            mock_nexus_artefact_handler.return_value
        )
        mock_nexus_artefact_handler_instance.download.return_value = [
            "asset1.tar.gz"
        ]
        mock_nexus_artefact_handler_instance.get_metadata.return_value = {
            "key": "value"
        }
        mock_check_result = CheckResult(
            "check-name", False, ":book:", 0, "", ""
        )
        run_checks_mock.return_value = [mock_check_result] * 3

        artefact = await nexus_handler.process_request(request_body)

        mock_nexus_artefact_handler_instance.download.assert_awaited_once_with(
            created_artefact
        )
        mock_nexus_artefact_handler_instance.get_metadata.assert_awaited_once_with(  # pylint: disable=line-too-long # noqa: E501
            created_artefact
        )
        run_checks_mock.assert_awaited_once_with(created_artefact)
        mock_nexus_artefact_handler_instance.upload.assert_awaited_once_with(
            artefact, quarantine_repo
        )
        mock_nexus_artefact_handler_instance.delete.assert_awaited_once_with(
            artefact
        )

        assert artefact.project == "test-repo"
        assert artefact.name == "test-component"
        assert artefact.type == ArtefactType.PYPI
        assert artefact.id == "1234"
        assert artefact.tag == "1.0.0"
        assert artefact.group == "test-group"
        assert artefact.channel == "test-channel"
        assert artefact.metadata == {"key": "value"}
        assert artefact.assets == ["asset1.tar.gz"]
        assert artefact.checks_results == [mock_check_result] * 3


@pytest.mark.asyncio
async def test_nexus_handler_process_request_compliant(nexus_handler):
    request_body = {
        "repositoryName": "test-repo",
        "component": {
            "name": "test-component",
            "format": "pypi",
            "componentId": "1234",
            "version": "1.0.0",
            "group": "test-group",
            "channel": "test-channel",
        },
    }

    with patch(
        "ska_cicd_artefact_validations.handlers.nexus.nexus_handler.NexusArtefactHandler",  # pylint: disable=line-too-long # noqa: E501
        autospec=True,
    ) as mock_nexus_artefact_handler, patch(
        "ska_cicd_artefact_validations.handlers.nexus.nexus_handler.run_checks",  # pylint: disable=line-too-long # noqa: E501
        new_callable=AsyncMock,
    ) as run_checks_mock, patch(
        "ska_cicd_artefact_validations.handlers.nexus.nexus_handler.NexusHandler._create_artefact_from_request",  # pylint: disable=line-too-long # noqa: E501
        new_callable=MagicMock,
    ) as create_artefact_mock:

        created_artefact = Artefact(
            project="test-repo",
            name="test-component",
            type=ArtefactType.PYPI,
            id="1234",
            tag="1.0.0",
            group="test-group",
            channel="test-channel",
        )
        create_artefact_mock.return_value = created_artefact

        mock_nexus_artefact_handler_instance = (
            mock_nexus_artefact_handler.return_value
        )
        mock_nexus_artefact_handler_instance.download.return_value = [
            "asset1.tar.gz"
        ]
        mock_nexus_artefact_handler_instance.get_metadata.return_value = {
            "key": "value"
        }
        mock_check_result = CheckResult(
            "check-name", True, ":book:", 0, "", ""
        )
        run_checks_mock.return_value = [mock_check_result] * 3

        artefact = await nexus_handler.process_request(request_body)

        mock_nexus_artefact_handler_instance.download.assert_awaited_once_with(
            created_artefact
        )
        mock_nexus_artefact_handler_instance.get_metadata.assert_awaited_once_with(  # pylint: disable=line-too-long # noqa: E501
            created_artefact
        )
        run_checks_mock.assert_awaited_once_with(created_artefact)
        mock_nexus_artefact_handler_instance.upload.assert_not_awaited()
        mock_nexus_artefact_handler_instance.delete.assert_not_awaited()

        assert artefact.project == "test-repo"
        assert artefact.name == "test-component"
        assert artefact.type == ArtefactType.PYPI
        assert artefact.id == "1234"
        assert artefact.tag == "1.0.0"
        assert artefact.group == "test-group"
        assert artefact.channel == "test-channel"
        assert artefact.metadata == {"key": "value"}
        assert artefact.assets == ["asset1.tar.gz"]
        assert artefact.checks_results == [mock_check_result] * 3
