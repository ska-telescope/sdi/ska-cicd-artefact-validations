# pylint: disable=W0613
import base64
import hashlib
import hmac

import pytest
from fastapi import FastAPI, Request
from fastapi.testclient import TestClient

from ska_cicd_artefact_validations.api.authentication import (
    FactoryAuthenticator,
    HmacSignatureAuthenticator,
    PasswordAuthenticator,
    TokenAuthenticator,
)


@pytest.fixture(name="env_vars")
def set_env_vars(monkeypatch):
    monkeypatch.setenv("TOKEN_ENV", "test-token")
    monkeypatch.setenv("USERNAME_ENV", "test-user")
    monkeypatch.setenv("PASSWORD_ENV", "test-pass")
    monkeypatch.setenv("HMAC_SECRET_ENV", "test-secret")


def create_app(authenticator):
    app = FastAPI()

    @app.get("/test")
    async def test_endpoint_get(request: Request):
        await authenticator.authenticate(request)
        return {"message": "success"}

    @app.post("/test")
    async def test_endpoint_post(request: Request):
        await authenticator.authenticate(request)
        return {"message": "success"}

    return app


def get_client(app):
    return TestClient(app)


# Helper function to generate Basic Auth headers
def generate_basic_auth_header(username, password):
    credentials = base64.b64encode(f"{username}:{password}".encode()).decode(
        "utf-8"
    )
    return {"authorization": f"Basic {credentials}"}


# Helper function to generate HMAC signature
def generate_hmac_signature(secret, data):
    return hmac.new(secret.encode(), data, hashlib.sha1).hexdigest()


# Test TokenAuthenticator
@pytest.mark.asyncio
async def test_token_authenticator_success(env_vars):
    config = {"token_header_env": "X-Token", "token_env": "TOKEN_ENV"}
    authenticator = TokenAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = {"X-Token": "test-token"}
    response = client.get("/test", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_token_authenticator_failure(env_vars):
    config = {"token_header_env": "X-Token", "token_env": "TOKEN_ENV"}
    authenticator = TokenAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = {"X-Token": "wrong-token"}
    response = client.get("/test", headers=headers)
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_token_authenticator_error(env_vars):
    config = {}
    with pytest.raises(
        TypeError, match="Token authentication fields are invalid."
    ):
        TokenAuthenticator(config)


# Test PasswordAuthenticator
@pytest.mark.asyncio
async def test_password_authenticator_success(env_vars):
    config = {"username_env": "USERNAME_ENV", "password_env": "PASSWORD_ENV"}
    authenticator = PasswordAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = generate_basic_auth_header("test-user", "test-pass")
    response = client.get("/test", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_password_authenticator_failure(env_vars):
    config = {"username_env": "USERNAME_ENV", "password_env": "PASSWORD_ENV"}
    authenticator = PasswordAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = generate_basic_auth_header("test-user", "wrong-pass")
    response = client.get("/test", headers=headers)
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_password_authenticator_no_auth(env_vars):
    config = {"username_env": "USERNAME_ENV", "password_env": "PASSWORD_ENV"}
    authenticator = PasswordAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = {}
    response = client.get("/test", headers=headers)
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_password_authenticator_error(env_vars):
    config = {}
    with pytest.raises(
        TypeError, match="Password authentication fields are invalid."
    ):
        PasswordAuthenticator(config)


# Test HmacSignatureAuthenticator
@pytest.mark.asyncio
async def test_hmac_signature_authenticator_success(env_vars):
    config = {
        "hmac_signature_header": "X-Hmac-Signature",
        "hmac_secret_env": "HMAC_SECRET_ENV",
    }
    authenticator = HmacSignatureAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    data = b"test-data"
    signature = generate_hmac_signature("test-secret", data)
    headers = {"X-Hmac-Signature": signature}
    response = client.post("/test", headers=headers, data=data)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_hmac_signature_authenticator_failure(env_vars):
    config = {
        "hmac_signature_header": "X-Hmac-Signature",
        "hmac_secret_env": "HMAC_SECRET_ENV",
    }
    authenticator = HmacSignatureAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    data = b"test-data"
    wrong_signature = generate_hmac_signature("test-secret", b"wrong-data")
    headers = {"X-Hmac-Signature": wrong_signature}
    response = client.post("/test", headers=headers, data=data)
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_hmac_signature_authenticator_error():
    config = {"hmac_secret_env": "HMAC_SECRET_ENV"}
    with pytest.raises(
        TypeError, match="Hmac Signature authentication fields are invalid."
    ):
        HmacSignatureAuthenticator(config)


# Test FactoryAuthenticator
@pytest.mark.asyncio
async def test_factory_authenticator_token(env_vars):
    config = {
        "auth_type": "token",
        "token_header_env": "X-Token",
        "token_env": "TOKEN_ENV",
    }
    authenticator = FactoryAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = {"X-Token": "test-token"}
    response = client.get("/test", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_factory_authenticator_password(env_vars):
    config = {
        "auth_type": "password",
        "username_env": "USERNAME_ENV",
        "password_env": "PASSWORD_ENV",
    }
    authenticator = FactoryAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    headers = generate_basic_auth_header("test-user", "test-pass")
    response = client.get("/test", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_factory_authenticator_hmac_signature(env_vars):
    config = {
        "auth_type": "hmac_signature",
        "hmac_signature_header": "X-Hmac-Signature",
        "hmac_secret_env": "HMAC_SECRET_ENV",
    }
    authenticator = FactoryAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)

    data = b"test-data"
    signature = generate_hmac_signature("test-secret", data)
    headers = {"X-Hmac-Signature": signature}
    response = client.post("/test", headers=headers, data=data)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_factory_authenticator_none():
    config = {"auth_type": "none"}
    authenticator = FactoryAuthenticator(config)
    app = create_app(authenticator)
    client = get_client(app)
    data = b"test-data"
    response = client.post("/test", data=data)
    assert response.status_code == 200
    assert response.json() == {"message": "success"}


@pytest.mark.asyncio
async def test_factory_authenticator_invalid_type():
    config = {"auth_type": "invalid"}
    with pytest.raises(
        ValueError, match="Authentication type field is invalid."
    ):
        FactoryAuthenticator(config)
