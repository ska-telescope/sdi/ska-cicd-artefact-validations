from unittest.mock import AsyncMock, patch

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.check import CheckResult
from ska_cicd_artefact_validations.tasks.main_task import main_task


@patch("ska_cicd_artefact_validations.tasks.main_task.insertdb_task")
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.create_merge_request_task"
)
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.HarborHandler",
    autospec=True,
)
def test_main_task_harbor(
    mock_HarborHandler, mock_create_merge_request, mock_insertdb
):
    request_body = {"some": "data"}
    request_type = "harbor"

    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )

    mock_HarborHandler.return_value.process_request = AsyncMock(
        return_value=artefact
    )

    main_task(request_body, request_type)

    mock_HarborHandler.assert_called_once()
    mock_HarborHandler.return_value.process_request.assert_called_once_with(
        request_body
    )
    mock_insertdb.delay.assert_called_once_with(artefact.to_json())
    mock_create_merge_request.delay.assert_called_once_with(artefact)


@patch("ska_cicd_artefact_validations.tasks.main_task.insertdb_task")
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.create_merge_request_task"
)
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.NexusHandler", autospec=True
)
def test_main_task_nexus(
    mock_NexusHandler, mock_create_merge_request, mock_insertdb
):
    request_body = {"some": "data"}
    request_type = "nexus"

    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )

    mock_NexusHandler.return_value.process_request = AsyncMock(
        return_value=artefact
    )

    main_task(request_body, request_type)

    mock_NexusHandler.assert_called_once()
    mock_NexusHandler.return_value.process_request.assert_called_once_with(
        request_body
    )
    mock_insertdb.delay.assert_called_once_with(artefact.to_json())
    mock_create_merge_request.delay.assert_called_once_with(artefact)


@patch("ska_cicd_artefact_validations.tasks.main_task.insertdb_task")
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.create_merge_request_task"
)
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.HarborHandler",
    autospec=True,
)
@patch(
    "ska_cicd_artefact_validations.tasks.main_task.NexusHandler", autospec=True
)
def test_main_task_invalid(
    mock_NexusHandler,
    mock_HarborHandler,
    mock_create_merge_request,
    mock_insertdb,
):
    request_body = {"some": "data"}
    request_type = "invalid"

    main_task(request_body, request_type)

    mock_HarborHandler.assert_not_called()
    mock_NexusHandler.assert_not_called()
    mock_insertdb.delay.assert_not_called()
    mock_create_merge_request.delay.assert_not_called()
