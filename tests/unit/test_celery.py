from unittest.mock import Mock, patch

import pytest
from celery import Celery

from ska_cicd_artefact_validations.mycelery import (
    app,
    log_task_failure,
    log_task_on_start,
    log_task_retry,
    log_task_success,
)


@pytest.fixture(name="celery_app")
def get_celery_app():
    return app


def test_celery_app_config(celery_app):
    assert isinstance(celery_app, Celery)
    assert celery_app.conf.task_serializer == "pickle"
    assert celery_app.conf.result_serializer == "pickle"
    assert "application/json" in celery_app.conf.accept_content
    assert "application/x-python-serialize" in celery_app.conf.accept_content


@patch("ska_cicd_artefact_validations.mycelery.logging")
def test_log_task_on_start(mock_logger):
    log_task_on_start("task_id_1", (1, 2), {"key": "value"})
    mock_logger.debug.assert_called_with(
        "Task Starting: %s",
        {"task_id": "task_id_1", "args": (1, 2), "kwargs": {"key": "value"}},
    )


@patch("ska_cicd_artefact_validations.mycelery.logging")
def test_log_task_failure(mock_logger):
    exc = Exception("Test Exception")
    einfo = Mock()
    log_task_failure(exc, "task_id_2", (3, 4), {"key": "value2"}, einfo)
    mock_logger.debug.assert_called_with(
        "Task Failed: %s",
        {
            "status": "Failed",
            "exception": "Test Exception",
            "task_id": "task_id_2",
            "task_args": (3, 4),
            "task_kwargs": {"key": "value2"},
            "exception_info": str(einfo),
        },
    )


@patch("ska_cicd_artefact_validations.mycelery.logging")
def test_log_task_retry(mock_logger):
    exc = Exception("Retry Exception")
    einfo = Mock()
    log_task_retry(exc, "task_id_3", (5, 6), {"key": "value3"}, einfo)
    mock_logger.debug.assert_called_with(
        "Task Retried: %s",
        {
            "status": "Retrying",
            "exception": "Retry Exception",
            "task_id": "task_id_3",
            "task_args": (5, 6),
            "task_kwargs": {"key": "value3"},
            "exception_info": str(einfo),
        },
    )


@patch("ska_cicd_artefact_validations.mycelery.logging")
def test_log_task_success(mock_logger):
    log_task_success("result_value", "task_id_4", (7, 8), {"key": "value4"})
    mock_logger.debug.assert_called_with(
        "Task Succeeded: %s",
        {
            "return_value": "result_value",
            "task_id": "task_id_4",
            "task_args": (7, 8),
            "task_kwargs": {"key": "value4"},
        },
    )
