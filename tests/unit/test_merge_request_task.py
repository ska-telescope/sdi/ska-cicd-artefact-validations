from unittest.mock import AsyncMock, patch

import pytest

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.core.check import CheckResult
from ska_cicd_artefact_validations.tasks.merge_request_task import (
    create_merge_request,
)


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.tasks.merge_request_task.GitLabApi",
    autospec=True,
)
async def test_create_merge_request_success(mock_api):
    mock_api.return_value.get_project_settings_info = AsyncMock(
        return_value={"default_branch": "main"}
    )
    mock_api.return_value.create_branch = AsyncMock(
        return_value={"name": "branch"}
    )
    mock_api.return_value.create_mr = AsyncMock(return_value={"iid": "1"})

    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )

    # Calling the function
    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )
    result = await create_merge_request(artefact)

    assert result == {"iid": "1"}


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.tasks.merge_request_task.GitLabApi",
    autospec=True,
)
async def test_create_merge_request_fail_branch_creation(mock_api):
    mock_api.return_value.get_project_settings_info = AsyncMock(
        return_value={"default_branch": "main"}
    )
    mock_api.return_value.create_branch = AsyncMock(return_value="Failed")
    mock_api.return_value.create_mr = AsyncMock(return_value={"iid": "1"})

    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )

    # Calling the function
    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )
    result = await create_merge_request(artefact)

    assert result is None


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.tasks.merge_request_task.GitLabApi",
    autospec=True,
)
async def test_create_merge_request_fail_mr_creation(mock_api):
    mock_api.return_value.get_project_settings_info = AsyncMock(
        return_value={"default_branch": "main"}
    )
    mock_api.return_value.create_branch = AsyncMock(
        return_value={"name": "branch"}
    )
    mock_api.return_value.create_mr = AsyncMock(return_value="Failed")

    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )

    # Calling the function
    artefact = Artefact(
        name="artefact-name",
        tag="v1.0.0",
        type=ArtefactType.OCI,
        checks_results=[CheckResult("check-name", False, ":book:", 0, "", "")],
        metadata={
            "CI_PROJECT_ID": "123",
            "CI_JOB_ID": "456",
            "GITLAB_USER_ID": "789",
        },
    )
    result = await create_merge_request(artefact)

    assert result is None
