Feature: Test Checks

    Scenario Outline: Check artefact name
        Given an Artefact with name attribute set to <value>
        Then check_artefact_name should return <result>

        Examples: Image Names
            | value | result |
            | ska-package | true |
            | ska-subsystem-module | true |
            | ska-module | true |
            | ska-mid-module | true |
            | ska-low-subsystem-module | true |
            | ska-subsystem-mid-module | true |
            | ska-valid-package | true |
            | ska-valid-mid | true |
            | ska-low | true |
            | ska-mid | true |
            | ska-valid-package | true |
            | ska-valid-package | true |
            | ska | false |
            | ska-- | false |
            | ska-mid- | false |
            | ska-invalid- | false |
            | ska-invalid- | false |
            | ska-invalid- | false |
            | ska-mid-module-to2to | true |
            | ska-valid-package-2to2 | true |
            | ska-valid-package-to2 | true |
            | ska-valid20-package | true |
            | ska-valid20-packag2e | true |


    Scenario Outline: Check artefact tag
        Given an Artefact with tag attribute set to <value>
        Then check_artefact_tag should return <result>

        Examples: Image Tags
            | value | result |
            | 0.0.4 | true |
            | 1.2.3 | true |
            | 10.20.30 | true |
            | 1.1.2-prerelease+meta | true |
            | 1.1.2+meta | true |
            | 1.1.2+meta-valid | true |
            | 1.0.0-alpha | true |
            | 1.0.0-beta | true |
            | 1.0.0-alpha.beta | true |
            | 1.0.0-alpha.beta.1 | true |
            | 1.0.0-alpha.1 | true |
            | 1.0.0-alpha0.valid | true |
            | 1.0.0-alpha.0valid | true |
            | 1.0.0-alpha-a.b-c-somethinglong+build.1-aef.1-its-okay | true |
            | 1.0.0-rc.1+build.1 | true |
            | 2.0.0-rc.1+build.123 | true |
            | 1.2.3-beta | true |
            | 10.2.3-DEV-SNAPSHOT | true |
            | 1.2.3-SNAPSHOT-123 | true |
            | 1.0.0 | true |
            | 2.0.0 | true |
            | 1.1.7 | true |
            | 2.0.0+build.1848 | true |
            | 2.0.1-alpha.1227 | true |
            | 1.0.0-alpha+beta | true |
            | 1.2.3----RC-SNAPSHOT.12.9.1--.12+788 | true |
            | 1.2.3----R-S.12.9.1--.12+meta | true |
            | 1.2.3----RC-SNAPSHOT.12.9.1--.12 | true |
            | 1.0.0+0.build.1-rc.10000aaa-kk-0.1 | true |
            | 99999999999999999999999.999999999999999999.99999999999999999 | true |
            | 1.0.0-0A.is.legal | true |
            | 1 | false |
            | 1.2 | false |
            | 1.2.3-0123 | false |
            | 1.2.3-0123.0123 | false |
            | 1.1.2+.123 | false |
            | +invalid | false |
            | -invalid | false |
            | -invalid+invalid | false |
            | -invalid.01 | false |
            | alpha | false |
            | alpha.beta | false |
            | alpha.beta.1 | false |
            | alpha.1 | false |
            | alpha+beta | false |
            | alpha_beta | false |
            | alpha. | false |
            | alpha.. | false |
            | beta | false |
            | 1.0.0-alpha_beta | false |
            | -alpha. | false |
            | 1.0.0-alpha.. | false |
            | 1.0.0-alpha..1 | false |
            | 1.0.0-alpha...1 | false |
            | 1.0.0-alpha....1 | false |
            | 1.0.0-alpha.....1 | false |
            | 1.0.0-alpha......1 | false |
            | 1.0.0-alpha.......1 | false |
            | 01.1.1 | false |
            | 1.01.1 | false |
            | 1.1.01 | false |
            | 1.2 | false |
            | 1.2.3.DEV | false |
            | 1.2-SNAPSHOT | false |
            | 1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12+788 | false |
            | 1.2-RC-SNAPSHOT | false |
            | -1.0.3-gamma+b7718 | false |
            | +justmeta | false |
            | 9.8.7+meta+meta | false |
            | 9.8.7-whatever+meta+meta | false |
            | 99999999999999999999999.999999999999999999.99999999999999999----RC-SNAPSHOT.12.09.1--------------------------------..12 | false |


    Scenario Outline: Check artefact tag
        Given an Artefact with tag attribute set to <value>
        And type attribute set to pypi
        Then check_artefact_tag should return <result>

        Examples: Image Tags
            | value | result |
            | 0.0.4 | true |
            | 1.2.3 | true |
            | 1!2.0.0 | true |
            | 1.0.0a1 | true |
            | 2.0.0b2 | true |
            | 3.1.0rc1 | true |
            | 4.0.0.post1 | true |
            | 1.2.0.dev3 | true |
            | 2!1.2.3 | true |
            | 1.0.0a1.post1.dev1 | true |
            | 2.0.1a1227 | true |
            | .1.0.0 | false |
            | 1.0. | false |
            | 01.0.0 | false |
            | 1.0.0a | false |  
            | 1.0..0 | false |
            | 1.0.0post | false |
            | 1.0.0.dev | false |
            | 1! | false |
            | 1.0.-1 | false |
            | 1.0.0-rc.1 | false |
            | 2.0.1-alpha.1227 | false |
            | 1.0.0-alpha.beta.1 | false |
    
    
    Scenario Outline: Check artefact metadata with valid metadata
        Given an Artefact with metadata attribute from tests/unit/harbor_hooks/artefact_metadata_valid.json
        Then check_artefact_metadata should return true

    Scenario Outline: Check artefact metadata with invalid metadata
        Given an Artefact with metadata attribute from tests/unit/harbor_hooks/artefact_metadata_invalid.json
        Then check_artefact_metadata should return false

    Scenario: Artefact with RAW type and valid tar files
        Given an Artefact with type attribute set to raw
        And assets attribute set to ["valid1.tar", "valid2.tar"]
        And create files for artefact assets
        Then check_raw_artefact_asset should return true
        Then delete files
