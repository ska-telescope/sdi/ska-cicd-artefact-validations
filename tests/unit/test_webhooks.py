# test_webhooks.py
import json
from unittest.mock import patch

from fastapi.testclient import TestClient

from ska_cicd_artefact_validations.main_api import app

client = TestClient(app)


def test_nexus_event_valid():
    with open(
        "tests/unit/nexus_hooks/nexus_hook_valid.json", "r", encoding="utf-8"
    ) as f:
        data = json.loads(f.read())

    with patch(
        "ska_cicd_artefact_validations.main_api.FactoryAuthenticator.authenticate",  # pylint: disable=line-too-long # noqa: E501
        return_value=None,
    ), patch("ska_cicd_artefact_validations.main_api.main_task.delay"):
        response = client.post("/nexus", json=data)
        assert response.status_code == 200
        assert response.json() == {"message": "OK"}


def test_nexus_event_invalid():
    with open(
        "tests/unit/nexus_hooks/nexus_hook_invalid.json", "r", encoding="utf-8"
    ) as f:
        data = json.loads(f.read())

    with patch(
        "ska_cicd_artefact_validations.main_api.FactoryAuthenticator.authenticate",  # pylint: disable=line-too-long # noqa: E501
        return_value=None,
    ), patch("ska_cicd_artefact_validations.main_api.main_task.delay"):
        response = client.post("/nexus", json=data)
        assert response.status_code == 400
        assert response.json() == {"message": "Rejected"}


def test_harbor_event_valid():
    with open(
        "tests/unit/harbor_hooks/harbor_hook_valid.json", "r", encoding="utf-8"
    ) as f:
        data = json.loads(f.read())

    with patch(
        "ska_cicd_artefact_validations.main_api.FactoryAuthenticator.authenticate",  # pylint: disable=line-too-long # noqa: E501
        return_value=None,
    ), patch("ska_cicd_artefact_validations.main_api.main_task.delay"):
        response = client.post("/harbor", json=data)
        assert response.status_code == 200
        assert response.json() == {"message": "OK"}


def test_harbor_event_invalid():
    with open(
        "tests/unit/harbor_hooks/harbor_hook_invalid.json",
        "r",
        encoding="utf-8",
    ) as f:
        data = json.loads(f.read())

    with patch(
        "ska_cicd_artefact_validations.main_api.FactoryAuthenticator.authenticate",  # pylint: disable=line-too-long # noqa: E501
        return_value=None,
    ), patch("ska_cicd_artefact_validations.main_api.main_task.delay"):
        response = client.post("/harbor", json=data)
        assert response.status_code == 400
        assert response.json() == {"message": "Rejected"}
