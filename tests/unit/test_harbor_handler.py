from unittest.mock import AsyncMock, MagicMock, call, patch

import pytest

from ska_cicd_artefact_validations.core.artefact import Artefact
from ska_cicd_artefact_validations.core.check import CheckResult
from ska_cicd_artefact_validations.handlers.harbor.harbor_handler import (
    HarborHandler,
)


@pytest.fixture(name="harbor_handler")
def get_harbor_handler():
    feature_toggler_mock = MagicMock()
    feature_toggler_mock.is_enabled.return_value = True
    with patch(
        "ska_cicd_artefact_validations.handlers.harbor.harbor_handler.HarborApi",  # pylint: disable=line-too-long # noqa: E501
        autospec=True,
    ) as mock_api:
        handler = HarborHandler(feature_toggler_mock)
        handler.api = mock_api
        yield handler


@pytest.mark.asyncio
async def test_get_artefact_data(harbor_handler):
    request_body = {
        "event_data": {
            "repository": {"namespace": "test-validations", "name": "busybox"},
            "resources": [{"tag": "v01"}],
        }
    }

    harbor_handler.api.get_artifact.return_value = AsyncMock(
        extra_attrs=MagicMock(config={"Labels": {"label": "test"}})
    )
    artefact = await harbor_handler._get_artefact_data(request_body)

    assert artefact.metadata == {"label": "test"}


@pytest.mark.asyncio
async def test_get_artefact_data_no_artefact(harbor_handler):
    request_body = {
        "event_data": {
            "repository": {"namespace": "test-validations", "name": "busybox"},
            "resources": [{"tag": "v01"}],
        }
    }

    harbor_handler.api.get_artifact = AsyncMock(
        side_effect=Exception("No artefact found")
    )
    artefact = await harbor_handler._get_artefact_data(request_body)

    assert isinstance(artefact, Exception)


@pytest.mark.asyncio
async def test_get_artefact_data_no_metadata(harbor_handler):
    request_body = {
        "event_data": {
            "repository": {"namespace": "test-validations", "name": "busybox"},
            "resources": [{"tag": "v01"}],
        }
    }

    harbor_handler.api.get_artifact.return_value = AsyncMock(extra_attrs={})
    artefact = await harbor_handler._get_artefact_data(request_body)

    assert artefact.metadata == {}


@pytest.mark.asyncio
async def test_add_label(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    mock_label = MagicMock()
    mock_label.model_dump.return_value = {"name": "test_label"}

    harbor_handler.api.get_labels.return_value = [mock_label]
    harbor_handler.api.add_artifact_label = AsyncMock()

    await harbor_handler._add_label(artefact, "test_label")

    harbor_handler.api.add_artifact_label.assert_awaited_once_with(
        "artefact-project",
        "artefact-name",
        "artefact-tag",
        {"name": "test_label"},
    )


@pytest.mark.asyncio
async def test_add_label_no_labels(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    mock_label = MagicMock()
    mock_label.model_dump.return_value = {"name": "test_label"}

    harbor_handler.api.get_labels = AsyncMock(side_effect=[[], [mock_label]])
    harbor_handler.api.create_label = AsyncMock()
    harbor_handler.api.add_artifact_label = AsyncMock()

    await harbor_handler._add_label(artefact, "test_label")

    harbor_handler.api.add_artifact_label.assert_awaited_once_with(
        "artefact-project",
        "artefact-name",
        "artefact-tag",
        {"name": "test_label"},
    )


@pytest.mark.asyncio
async def test_add_label_already_exists(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    mock_label = MagicMock(name="test_label")
    mock_label.model_dump.return_value = {"name": "test_label"}

    harbor_handler.api.get_labels.return_value = [mock_label]
    harbor_handler.api.add_artifact_label = AsyncMock(
        side_effect=Exception("Artefact already exists")
    )

    await harbor_handler._add_label(artefact, "test_label")


@pytest.mark.asyncio
async def test_add_labels(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    mock_labels = ["label1", "label2"]
    harbor_handler._add_label = AsyncMock()

    await harbor_handler._add_labels(artefact, mock_labels)

    expected_calls = [call(artefact, label) for label in mock_labels]
    harbor_handler._add_label.assert_has_calls(expected_calls)
    assert harbor_handler._add_label.call_count == len(mock_labels)


@pytest.mark.asyncio
async def test_check_vulnerability_report(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    report = MagicMock()
    report.vulnerabilities = []
    report.severity = MagicMock()
    report.severity.value = "Critical"

    harbor_handler.api.get_artifact_vulnerabilities = AsyncMock(
        return_value=report
    )

    (
        vulnerable,
        vulnerability_report,
    ) = await harbor_handler._check_vulnerability_report(artefact)

    harbor_handler.api.get_artifact_vulnerabilities.assert_called_once_with(
        artefact.project, artefact.name, artefact.tag
    )
    assert vulnerable is True
    assert vulnerability_report == {"vulnerability_report": []}


@pytest.mark.asyncio
async def test_check_vulnerability_report_not_compiled(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )

    report = MagicMock()
    report.vulnerabilities = []
    report.severity = MagicMock()
    report.severity.value = "Critical"

    harbor_handler.api.get_artifact_vulnerabilities = AsyncMock(
        side_effect=[Exception("Report not Compiled"), report]
    )

    with patch("time.sleep", return_value=None):
        (
            vulnerable,
            vulnerability_report,
        ) = await harbor_handler._check_vulnerability_report(artefact)

    expected_calls = [
        call(artefact.project, artefact.name, artefact.tag) for i in range(2)
    ]
    harbor_handler.api.get_artifact_vulnerabilities.assert_has_calls(
        expected_calls
    )
    assert vulnerable is True
    assert vulnerability_report == {"vulnerability_report": []}


@pytest.mark.asyncio
async def test_check_vulnerability_report_not_compiled_max(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )

    report = MagicMock()
    report.vulnerabilities = []
    report.severity = MagicMock()
    report.severity.value = "Critical"

    return_values = [
        Exception("Report not Compiled")
    ] * harbor_handler.config.query_retries
    harbor_handler.api.get_artifact_vulnerabilities = AsyncMock(
        side_effect=return_values
    )

    with patch("time.sleep", return_value=None):
        (
            vulnerable,
            vulnerability_report,
        ) = await harbor_handler._check_vulnerability_report(artefact)

    expected_calls = [
        call(artefact.project, artefact.name, artefact.tag)
        for i in range(len(return_values))
    ]
    harbor_handler.api.get_artifact_vulnerabilities.assert_has_calls(
        expected_calls
    )
    assert vulnerable is True
    assert vulnerability_report == {"vulnerability_report": []}


@pytest.mark.asyncio
async def test_check_vulnerability_report_no_vulnerability(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    report = MagicMock()
    report.vulnerabilities = []
    report.severity.value = "Unknown"

    harbor_handler.api.get_artifact_vulnerabilities = AsyncMock(
        return_value=report
    )

    (
        vulnerable,
        vulnerability_report,
    ) = await harbor_handler._check_vulnerability_report(artefact)

    harbor_handler.api.get_artifact_vulnerabilities.assert_called_once_with(
        artefact.project, artefact.name, artefact.tag
    )
    assert vulnerable is False
    assert vulnerability_report == {"vulnerability_report": []}


@pytest.mark.asyncio
async def test_promote_with_no_delete(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )

    target_project = "test_target_project"

    harbor_handler.api.copy_artifact = AsyncMock()

    await harbor_handler._promote(artefact, target_project)

    harbor_handler.api.copy_artifact.assert_awaited_once_with(
        target_project,
        artefact.name,
        f"{artefact.project}/{artefact.name}:{artefact.tag}",
    )


@pytest.mark.asyncio
async def test_promote_with_no_delete_exception(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )

    target_project = "test_target_project"

    harbor_handler.api.copy_artifact = AsyncMock(
        side_effect=Exception("Failed to copy")
    )

    await harbor_handler._promote(artefact, target_project)

    harbor_handler.api.copy_artifact.assert_awaited_once_with(
        target_project,
        artefact.name,
        f"{artefact.project}/{artefact.name}:{artefact.tag}",
    )


@pytest.mark.asyncio
async def test_promote_with_delete(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )

    target_project = "test_target_project"

    harbor_handler.api.copy_artifact = AsyncMock()
    harbor_handler.api.delete_artifact = AsyncMock()

    await harbor_handler._promote(artefact, target_project, delete_source=True)

    harbor_handler.api.copy_artifact.assert_awaited_once_with(
        target_project,
        artefact.name,
        f"{artefact.project}/{artefact.name}:{artefact.tag}",
    )
    harbor_handler.api.delete_artifact.assert_awaited_once_with(
        artefact.project, artefact.name, artefact.tag
    )


@pytest.mark.asyncio
async def test_promote_with_delete_exception(harbor_handler):
    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )

    target_project = "test_target_project"

    harbor_handler.api.copy_artifact = AsyncMock()
    harbor_handler.api.delete_artifact = AsyncMock(
        side_effect=Exception("Failed to delete")
    )

    await harbor_handler._promote(artefact, target_project, delete_source=True)

    harbor_handler.api.copy_artifact.assert_awaited_once_with(
        target_project,
        artefact.name,
        f"{artefact.project}/{artefact.name}:{artefact.tag}",
    )
    harbor_handler.api.delete_artifact.assert_awaited_once_with(
        artefact.project, artefact.name, artefact.tag
    )


@pytest.mark.asyncio
async def test_process_request_failed_checks(harbor_handler):
    request_body = {
        "event_data": {
            "repository": {"namespace": "test-validations", "name": "busybox"},
            "resources": [{"tag": "v01"}],
        }
    }

    check_result_mock = CheckResult("check-name", False, ":book:", 0, "", "")

    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    harbor_handler.check_vulnerabilities = True
    harbor_handler._get_artefact_data = AsyncMock(return_value=artefact)

    run_checks_mock = patch(
        "ska_cicd_artefact_validations.handlers.harbor.harbor_handler.run_checks",  # pylint: disable=line-too-long # noqa: E501
        new_callable=AsyncMock,
    )
    run_checks_mock.return_value = [check_result_mock]

    harbor_handler._add_labels = AsyncMock()
    harbor_handler._check_vulnerability_report = AsyncMock(
        return_value=(True, "artefact-report")
    )

    artefact = await harbor_handler.process_request(request_body)

    assert artefact.project == "artefact-project"
    assert artefact.name == "artefact-name"
    assert artefact.tag == "artefact-tag"
    assert artefact.report == "artefact-report"


@pytest.mark.asyncio
async def test_process_request(harbor_handler):
    request_body = {
        "event_data": {
            "repository": {"namespace": "test-validations", "name": "busybox"},
            "resources": [{"tag": "v01"}],
        }
    }

    check_result_mock = CheckResult("check-name", True, ":book:", 0, "", "")

    artefact = Artefact(
        name="artefact-name", project="artefact-project", tag="artefact-tag"
    )
    target_project = harbor_handler.config.promotion_repository

    harbor_handler._get_artefact_data = AsyncMock(return_value=artefact)

    with patch(
        "ska_cicd_artefact_validations.handlers.harbor.harbor_handler.run_checks",  # pylint: disable=line-too-long # noqa: E501
        new_callable=AsyncMock,
    ) as run_checks_mock:
        run_checks_mock.return_value = [check_result_mock]

        harbor_handler._add_labels = AsyncMock()
        harbor_handler._check_vulnerability_report = AsyncMock(
            return_value=(False, "artefact-report")
        )
        harbor_handler._promote = AsyncMock()

        artefact = await harbor_handler.process_request(request_body)

        harbor_handler._promote.assert_awaited_once_with(
            artefact,
            target_project,
            delete_source=harbor_handler.config.delete,
        )
        assert artefact.project == "artefact-project"
        assert artefact.name == "artefact-name"
        assert artefact.tag == "artefact-tag"
        assert artefact.report == "artefact-report"
