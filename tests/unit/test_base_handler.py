from unittest.mock import AsyncMock, MagicMock, mock_open, patch

import pytest

from ska_cicd_artefact_validations.core.artefact import Artefact, ArtefactType
from ska_cicd_artefact_validations.handlers.nexus.artefact_handlers import (
    BaseHandler,
)


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.AssetDownloader"  # pylint: disable=line-too-long # noqa: E501
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_download(mock_nexus_api, mock_session, mock_downloader):
    # Arrange
    artefact_id = "test-artefact-id"
    artefact = Artefact(id=artefact_id)
    mock_downloader_instance = mock_downloader.return_value
    mock_downloader_instance.download = AsyncMock(
        return_value=["/tmp/assets/test-asset.tar.gz"]
    )

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Act
    result = await handler.download(artefact)

    # Assert
    assert len(result) == 1
    assert result[0] == "/tmp/assets/test-asset.tar.gz"
    mock_downloader.assert_called_once_with(handler.api, handler.session)
    mock_downloader_instance.download.assert_called_once_with(artefact.id)


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.open",
    new_callable=mock_open,
    read_data="data",
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_upload_success(mock_api, mock_session, mock_open_file):
    artefact = Artefact(
        id="test-artefact-id",
        type=ArtefactType.PYPI,
        assets=["/path/to/asset1.tar.gz", "/path/to/asset2.tar.gz"],
    )
    target_repository = "test-repository"

    mock_api_instance = mock_api.return_value
    mock_api_instance.upload_component = AsyncMock(
        return_value={"result": "successfull"}
    )

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, target_repository)

    assert result is True
    # Ensure two uploads happened
    assert mock_api_instance.upload_component.call_count == 2
    # Check if file was opened for reading
    mock_open_file.assert_called()
    # Check the file handle was passed correctly
    mock_api_instance.upload_component.assert_any_call(
        repository_type=artefact.type.value,
        repository=target_repository,
        **{"asset": mock_open_file.return_value}
    )


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_upload_with_no_assets(mock_api, mock_session):
    artefact = Artefact(
        id="test-artefact-id", type=ArtefactType.PYPI, assets=[]
    )
    target_repository = "test-repository"

    mock_api_instance = mock_api.return_value
    mock_api_instance.upload_component = AsyncMock(
        return_value={"result": "successfull"}
    )

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, target_repository)

    assert result is None
    # Ensure no upload was attempted
    mock_api_instance.upload_component.assert_not_called()


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.open",
    new_callable=mock_open,
    read_data="data",
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_upload_with_partial_failure(
    mock_api, mock_session, mock_open_file
):
    artefact = Artefact(
        id="test-artefact-id",
        type=ArtefactType.PYPI,
        assets=["/path/to/asset1.tar.gz", "/path/to/asset2.tar.gz"],
    )
    target_repository = "test-repository"

    mock_api_instance = mock_api.return_value
    mock_api_instance.upload_component = AsyncMock(side_effect=[True, False])

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, target_repository)

    assert result is False
    # Ensure two uploads happened
    assert mock_api_instance.upload_component.call_count == 2
    # Check if file was opened for reading
    mock_open_file.assert_called()
    # Check the file handle was passed correctly
    mock_api_instance.upload_component.assert_any_call(
        repository_type=artefact.type.value,
        repository=target_repository,
        **{"asset": mock_open_file.return_value}
    )


@pytest.mark.asyncio
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.open",
    new_callable=mock_open,
    read_data="data",
)
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.NexusApi"
)
async def test_upload_with_all_failure(mock_api, mock_session, mock_open_file):
    artefact = Artefact(
        id="test-artefact-id",
        type=ArtefactType.PYPI,
        assets=["/path/to/asset1.tar.gz", "/path/to/asset2.tar.gz"],
    )
    target_repository = "test-repository"

    mock_api_instance = mock_api.return_value
    mock_api_instance.upload_component = AsyncMock(return_value=False)

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Act
    result = await handler.upload(artefact, target_repository)

    assert result is False
    # Ensure two uploads happened
    assert mock_api_instance.upload_component.call_count == 2
    # Check if file was opened for reading
    mock_open_file.assert_called()
    # Check the file handle was passed correctly
    mock_api_instance.upload_component.assert_any_call(
        repository_type=artefact.type.value,
        repository=target_repository,
        **{"asset": mock_open_file.return_value}
    )


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch("os.remove")
async def test_delete_downloaded_components(mock_os_remove, mock_session):
    # Arrange
    artefact_id = "test-artefact-id"
    artefact = Artefact(
        id=artefact_id,
        assets=["/path/to/asset1.tar.gz", "/path/to/asset2.tar.gz"],
    )

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Act
    await handler.delete_downloaded_components(artefact)

    # Assert

    # Ensure os.remove was called twice
    assert mock_os_remove.call_count == 2
    mock_os_remove.assert_any_call("/path/to/asset1.tar.gz")
    mock_os_remove.assert_any_call("/path/to/asset2.tar.gz")


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch("os.remove", side_effect=FileNotFoundError)
async def test_delete_components_with_missing_files(
    mock_os_remove, mock_session
):
    # Arrange
    artefact_id = "test-artefact-id"
    artefact = Artefact(
        id=artefact_id,
        assets=[
            "/path/to/missing_asset1.tar.gz",
            "/path/to/missing_asset2.tar.gz",
        ],
    )

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    # Assert FileNotFoundError is raised
    with pytest.raises(FileNotFoundError):
        # Act
        await handler.delete_downloaded_components(artefact)


@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.tarfile.open"  # pylint: disable=line-too-long # noqa: E501
)
def test_extract_metadata_from_tar_successful(mock_tarfile_open, mock_session):
    # Arrange
    tar_path = "/path/to/tarfile.tar"

    manifest_content = "key1=value1\nkey2=value2\n"
    expected_content = {"key1": "value1", "key2": "value2"}

    tarinfo = MagicMock()
    tarinfo.path = "MANIFEST"

    mock_archive = MagicMock()
    mock_archive.getmembers.return_value = [tarinfo]

    mock_tarfile_open.return_value.__enter__.return_value = mock_archive

    manifest_file = MagicMock()
    manifest_file.__enter__.return_value.read.return_value = (
        manifest_content.encode("utf-8")
    )
    mock_archive.extractfile.return_value = manifest_file

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    metadata = handler._extract_metadata_from_tar(tar_path)

    assert metadata == expected_content

    # Ensure tarfile open is called once
    mock_tarfile_open.assert_called_once()

    mock_archive.getmembers.assert_called_once()
    mock_archive.extractfile.assert_called_once()

    # Ensure MANIFEST file is read
    manifest_file.__enter__.return_value.read.assert_called_once()


@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.tarfile.open",  # pylint: disable=line-too-long # noqa: E501
    side_effect=FileNotFoundError,
)
def test_extract_metadata_from_tar_not_found(mock_tarfile_open, mock_session):
    # Arrange
    tar_path = "/path/to/tarfile.tar"

    manifest_content = "key1=value1\nkey2=value2\n"

    tarinfo = MagicMock()
    tarinfo.path = "MANIFEST"

    mock_archive = MagicMock()
    mock_archive.getmembers.return_value = [tarinfo]

    manifest_file = MagicMock()
    manifest_file.__enter__.return_value.read.return_value = (
        manifest_content.encode("utf-8")
    )
    mock_archive.extractfile.return_value = manifest_file

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    metadata = handler._extract_metadata_from_tar(tar_path)

    assert metadata is None

    # Ensure tarfile open is called
    mock_tarfile_open.assert_called_once()

    mock_archive.getmembers.assert_not_called()
    mock_archive.extractfile.assert_not_called()

    # Ensure MANIFEST file is read
    manifest_file.__enter__.return_value.read.assert_not_called()


@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.tarfile.open"  # pylint: disable=line-too-long # noqa: E501
)
def test_extract_metadata_from_tar_no_manifest(
    mock_tarfile_open, mock_session
):
    # Arrange
    tar_path = "/path/to/tarfile.tar"

    manifest_content = "key1=value1\nkey2=value2\n"

    tarinfo = MagicMock()
    tarinfo.path = "NOMAN"

    mock_archive = MagicMock()
    mock_archive.getmembers.return_value = [tarinfo]

    mock_tarfile_open.return_value.__enter__.return_value = mock_archive

    manifest_file = MagicMock()
    manifest_file.__enter__.return_value.read.return_value = (
        manifest_content.encode("utf-8")
    )
    mock_archive.extractfile.return_value = manifest_file

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    metadata = handler._extract_metadata_from_tar(tar_path)

    assert metadata is None

    # Ensure tarfile open is called once
    mock_tarfile_open.assert_called_once()

    mock_archive.getmembers.assert_called_once()
    mock_archive.extractfile.assert_not_called()

    # Ensure MANIFEST file is read
    manifest_file.__enter__.return_value.read.assert_not_called()


@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ZipFile"
)
def test_extract_metadata_from_zip_successful(mock_zipfile_open, mock_session):
    # Arrange
    zip_path = "/path/to/zipfile.zip"

    manifest_content = "key1=value1\nkey2=value2\n"
    expected_content = {"key1": "value1", "key2": "value2"}

    mock_zip_file = MagicMock()
    mock_zip_file.namelist.return_value = ["MANIFEST"]

    mock_zipfile_open.return_value.__enter__.return_value = mock_zip_file

    manifest_file = MagicMock()
    manifest_file.__enter__.return_value.read.return_value = (
        manifest_content.encode("utf-8")
    )
    mock_zip_file.open.return_value = manifest_file

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    metadata = handler._extract_metadata_from_zip(zip_path)

    assert metadata == expected_content

    # Ensure zipfile open is called once
    mock_zipfile_open.assert_called_once()

    mock_zip_file.namelist.assert_called_once()
    mock_zip_file.open.assert_called_once()

    # Ensure MANIFEST file is read
    manifest_file.__enter__.return_value.read.assert_called_once()


@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ZipFile",
    side_effect=FileNotFoundError,
)
def test_extract_metadata_from_zip_not_found(mock_zipfile_open, mock_session):
    # Arrange
    zip_path = "/path/to/zipfile.zip"

    manifest_content = "key1=value1\nkey2=value2\n"

    mock_zip_file = MagicMock()
    mock_zip_file.namelist.return_value = ["MANIFEST"]

    mock_zipfile_open.return_value.__enter__.return_value = mock_zip_file

    manifest_file = MagicMock()
    manifest_file.__enter__.return_value.read.return_value = (
        manifest_content.encode("utf-8")
    )
    mock_zip_file.open.return_value = manifest_file

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    metadata = handler._extract_metadata_from_zip(zip_path)

    assert metadata is None

    # Ensure zipfile open is called once
    mock_zipfile_open.assert_called_once()

    mock_zip_file.namelist.assert_not_called()
    mock_zip_file.open.assert_not_called()

    # Ensure MANIFEST file is not read
    manifest_file.__enter__.return_value.read.assert_not_called()


@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.ZipFile",
)
def test_extract_metadata_from_zip_no_manifest(
    mock_zipfile_open, mock_session
):
    # Arrange
    zip_path = "/path/to/zipfile.zip"

    manifest_content = "key1=value1\nkey2=value2\n"

    mock_zip_file = MagicMock()
    mock_zip_file.namelist.return_value = ["NOMAN"]

    mock_zipfile_open.return_value.__enter__.return_value = mock_zip_file

    manifest_file = MagicMock()
    manifest_file.__enter__.return_value.read.return_value = (
        manifest_content.encode("utf-8")
    )
    mock_zip_file.open.return_value = manifest_file

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    metadata = handler._extract_metadata_from_zip(zip_path)

    assert metadata is None

    # Ensure zipfile open is called once
    mock_zipfile_open.assert_called_once()

    mock_zip_file.namelist.assert_called_once()
    mock_zip_file.open.assert_not_called()

    # Ensure MANIFEST file is not read
    manifest_file.__enter__.return_value.read.assert_not_called()


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_tar"  # pylint: disable=line-too-long # noqa: E501
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_zip"  # pylint: disable=line-too-long # noqa: E501
)
async def test_get_metadata_tar_and_whl_identical(
    mock_zip, mock_tar, mock_session
):
    # Arrange
    artefact = Artefact(
        id="test-artefact-id",
        assets=["/path/to/asset.tar.gz", "/path/to/asset.whl"],
    )
    mock_tar.return_value = {"key": "value"}
    mock_zip.return_value = {"key": "value"}

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    result = await handler.get_metadata(artefact)

    assert result == {"key": "value"}
    mock_tar.assert_called_once_with("/path/to/asset.tar.gz")
    mock_zip.assert_called_once_with("/path/to/asset.whl")


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_tar"  # pylint: disable=line-too-long # noqa: E501
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_zip"  # pylint: disable=line-too-long # noqa: E501
)
async def test_get_metadata_tar_and_whl_different(
    mock_zip, mock_tar, mock_session
):
    # Arrange
    artefact = Artefact(
        id="test-artefact-id",
        assets=["/path/to/asset.tar.gz", "/path/to/asset.whl"],
    )
    mock_tar.return_value = {"key": "value1"}
    mock_zip.return_value = {"key": "value2"}

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    result = await handler.get_metadata(artefact)

    assert result == {}
    mock_tar.assert_called_once_with("/path/to/asset.tar.gz")
    mock_zip.assert_called_once_with("/path/to/asset.whl")


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_tar"  # pylint: disable=line-too-long # noqa: E501
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_zip"  # pylint: disable=line-too-long # noqa: E501
)
async def test_get_metadata_only_tar(mock_zip, mock_tar, mock_session):
    # Arrange
    artefact = Artefact(
        id="test-artefact-id", assets=["/path/to/asset.tar.gz"]
    )
    mock_tar.return_value = {"key": "value"}
    mock_zip.return_value = None

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    result = await handler.get_metadata(artefact)

    assert result == {"key": "value"}
    mock_tar.assert_called_once_with("/path/to/asset.tar.gz")
    mock_zip.assert_not_called()


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_tar"  # pylint: disable=line-too-long # noqa: E501
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_zip"  # pylint: disable=line-too-long # noqa: E501
)
async def test_get_metadata_only_zip(mock_zip, mock_tar, mock_session):
    # Arrange
    artefact = Artefact(id="test-artefact-id", assets=["/path/to/asset.whl"])
    mock_tar.return_value = None
    mock_zip.return_value = {"key": "value"}

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    result = await handler.get_metadata(artefact)

    assert result == {"key": "value"}
    mock_tar.assert_not_called()
    mock_zip.assert_called_once_with("/path/to/asset.whl")


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_tar"  # pylint: disable=line-too-long # noqa: E501
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_zip"  # pylint: disable=line-too-long # noqa: E501
)
async def test_get_metadata_both_none(mock_zip, mock_tar, mock_session):
    # Arrange
    artefact = Artefact(
        id="test-artefact-id",
        assets=["/path/to/asset.tar.gz", "/path/to/asset.whl"],
    )
    mock_tar.return_value = None
    mock_zip.return_value = None

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)
    # Act
    result = await handler.get_metadata(artefact)

    assert result is None
    mock_tar.assert_called_once_with("/path/to/asset.tar.gz")
    mock_zip.assert_called_once_with("/path/to/asset.whl")


@pytest.mark.asyncio
@patch("aiohttp.ClientSession")
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_tar"  # pylint: disable=line-too-long # noqa: E501
)
@patch(
    "ska_cicd_artefact_validations.handlers.nexus.artefact_handlers.BaseHandler._extract_metadata_from_zip"  # pylint: disable=line-too-long # noqa: E501
)
async def test_get_metadata_no_assets(mock_zip, mock_tar, mock_session):
    # Arrange
    artefact = Artefact(id="test-artefact-id")

    # Create a BaseHandler instance
    handler = BaseHandler(session=mock_session)

    with pytest.raises(TypeError):
        # Act
        _ = await handler.get_metadata(artefact)

    mock_tar.assert_not_called()
    mock_zip.assert_not_called()
