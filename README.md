
## Project Overview

This project provides a service for validating artefacts across repository such as Harbor and Nexus. It automates validation processes, ensuring compliance with established naming conventions, tag formats, and metadata requirements, using Celery tasks for asynchronous execution.

### Service flow
The service is triggered through a FastAPI endpoint, receiving hook data from Harbor or Nexus. The hook data is validated according to a pydantic model of the request and the expected values. For example, for Nexus, requests pertaining to an artefact deletion ("action": "DELETED") are deemed invalid and ignored, returning `400 Bad Request`. When a request is valid, `200 OK` is returned and Celery's "Main Task" is triggered, executing the Harbor or Nexus handlers to process the request and run the artefact checks.

After the validation flow is completed, Celery's Database Task and Merge Request Task are executed in parallel to add the artefact data to the database and to trigger the creation of a Merge Request in the artefact's Gitlab repository if it is non-compliant.

<div align="center">
<img src="docs/src/_static/img/validations_flow.jpg" align="center">
</div>
</br>


## Local Development

### General Workflow

Update and initialize the submodules used in the project:
```console
git submodule update --init --recursive
```

Install [Poetry](https://python-poetry.org/) for Python package and environment management.
```console
curl -sSL https://install.python-poetry.org | python3 -
```

To create the virtual environment and install dependencies:
```console
poetry install
```

You can enter the virtual environment with ```poetry shell``` and leave it with ```deactivate```.

Next you need to define the following environment variables in your ```PrivateRules.mak```:

```console
GITLAB_API_REQUESTER=marvin-42-service
GITLAB_API_PRIVATE_TOKEN=
NEXUS_URL=https://artefact.skao.int
NEXUS_API_USERNAME=validations
NEXUS_API_PASSWORD=
MONGO_DB=Artifact_Metadata
MONGO_COL=Artefact_Validation
MONGO_URL=mongodb://<user>:<pwd>@host.docker.internal:27017/
REDIS_PASSWORD=
UNLEASH_API_URL=https://gitlab.com/api/v4/feature_flags/unleash/26389323
UNLEASH_INSTANCE_ID=
UNLEASH_ENVIRONMENT=production-celery
NEXUS_HMAC_SIGNATURE_SECRET=
HARBOR_URL=https://harbor.skao.int/api/v2.0
HARBOR_USERNAME=admin
HARBOR_PASSWORD=
HARBOR_TOKEN=
MONGO_ROOT_PASSWORD=
```
Now, the project is ready for local development.

### Deploying the project locally

#### Deploying to local cluster
Make sure the repository has no outstanding changes before deploying.
The project can be deployed using the ```make k8s-install-chart``` target.
By default this will deploy the latest image in the OCI registry. You can specify the image version with `VERSION=0.18.2`.
If you wish to deploy a local image, modify the image specified in the deployment file.

Please note that some modules may break due to incorrect environment variables. You may need to modify the values in the ```PrivateRules.mak```.

#### Live Testing
To trigger the validation process on a published artefact, expose the service port using your preferred tunneling service (eg. Ngrok, localtunnel, loophole.cloud, etc.). Next you need to configure the webhook on Harbor/Nexus side.

Harbor:
https://harbor.skao.int/harbor/projects/4/webhook, click on ```NEW WEBHOOK``` and add the following settings:

```
Name: local
Notify type: http
Event type: Artifact pushed
Endpoint URL: <URL provided by tunneling service>/harbor
Auth Header: <Vault: aws-eu-west-2/production/cicd_validations/harbor_token>
```

Nexus:
https://artefact.skao.int/#admin/system/capabilities, click on ```Create Capability```, select ```Create Webhook: Repository Capability``` and add the following settings:

```
Repository: <A test repository (pypi-test for example, or a newly created one)>
Event types -> Selected: component
URL: <URL provided by tunneling service>/nexus
Secret Key: <Vault: aws-eu-west-2/production/cicd_validations/nexus_hmac_signature_secret>
```

Keep in mind that these webhooks may be already defined so you only need to change the URL, please check if they already exist before creating a new one.

Now you need to publish artefacts to these repositories in order to trigger the webhook.
## Checks

The checks that are currently implemented are:

- **Naming Convention:** described in [developer portal](https://developer.skatelescope.org/en/latest/explanation/central-artefact-repository.html#artefact-naming)
- **Tag Convention:** described in [developer portal](https://developer.skatelescope.org/en/latest/explanation/central-artefact-repository.html#artefact-versioning)
- **Metadata:** described in [developer portal](https://developer.skatelescope.org/en/latest/reference/software-release-package-references.html#metadata)

### How to Add a New Check

Each new check must use the abstract base class, [Check](https://gitlab.com/ska-telescope/sdi/ska-cicd-artefact-validations/-/blob/st-1946-refactor-validations/src/ska_cicd_artefact_validations/core/check.py), to ensure to define its `check` action, which performs the actual checking on the artefact and returns a boolean indicating the result. Example Check:

```python
from ska_cicd_artefact_validations.core.artefact import Artefact
from ska_cicd_artefact_validations.core.check import Check
from ska_cicd_artefact_validations.core.logging import logging
from ska_cicd_artefact_validations.core.message_generator import MessageType

# The name of the class must start with "Check"
class CheckThisIsANewCheck(Check):
    # This toggle must be added to https://gitlab.com/ska-telescope/sdi/ska-cicd-artefact-validations/-/feature_flags
    feature_toggle = "new_check_toggle"

    # Add what APIs you need to the __init__ function, but make sure to use type hinting!
    # Example: def __init__(self, api: GitLabApi, rtd_api: ReadTheDocsApi, jira_api: JiraApi, logger_name: str):
    def __init__(self):
        pass

    async def check(self, artefact: Artefact):

        # True means the check passed and there is no problem
        # False will cause validation to fail
        did_check_pass = True or False

        return did_check_pass

    async def type(self) -> MessageType:
        # This determines the type of message Marvin will issue:

        # Available message types:
        # MessageType.FAILURE - Blocks the MR
        # MessageType.WARNING - Does not block
        # MessageType.INFO

        # You can change the type according to the severity of the check:
        # Example:
        # if self.bad_problem:
        #   return MessageType.FAILURE
        # else:
        #   return MessageType.WARNING

        return MessageType.FAILURE

    async def description(self) -> str:
        return "Description that appears in the Marvin comment"

    async def mitigation_strategy(self) -> str:
        return "Message which should tell the user how to fix the problem"

```

Make sure the check filename starts with ```check_``` and is located inside ```src/ska_cicd_artefact_validations/checks```. After the new check is implemented, it will be added automatically to the list of checks to run (```import_check_classes()``` is responsible for that).

Then the necessary tests for the added checks should be added in [tests](tests/unit/) folder. These tests should get picked up by the main frameworks testing. The test can be run with ```make python-test```.

## Artefact Types
The service provides validation for the following artefact types:
- **Nexus**: Helm, Raw, Conan, Pypi
- **Harbor**: OCI

Artefacts are generalized between different types, so that they can be validated using the same checks.
An artefact is defined by the following fields:
- **project** - The repository/project of the artefact
- **name** - The name of the artefact without a version
- **tag** - The tag/version of the artefact
- **type** - The type (HELM, RAW, CONAN, PYPI, OCI)
- **id** - The ID of the artefact (Nexus only)
- **metadata** - The metadata (labels) included in the artefact
- **assets** - List of files that the artefact contains
- **checks_results** - The result of the checks for the artefact
- **report** - The vulnerabity report (Harbor only)
- **group** - The artefact group (Conan only)
- **channel** - The artefact channel (Conan only)


## Repository structure

- **api/**
    - Contains api related authentication, configuration and validation logic.

- **checks/**
    - Hosts the checks that run for each artefact

- **core/**
    - Central module for core functionalities of the project.
    - Contains essential scripts for artefact handling, configuration management, logging, and utility functions.

- **handlers/**
    - Divided into subdirectories for different repository types (e.g., `harbor`, `nexus`).
    - Each subdirectory includes specific configuration and handler scripts for managing artefacts in those repositories.

- **metrics/**
    - Contains the prometheus exporter metrics class and associated configuration

- **tasks/**
    - Contains the celery tasks that comprise the workflow of the service

## Service Configuration
The service uses file-based configuration, defined in the ```values.yaml``` file:

```yaml
config:
  harbor:
    promotion_repository: "production" # Target repository for promotion
    query_retries: 10 # Maximum number of retries when checking vulnerability report
    query_retry_delay_s: 10 # Delay between retries
    vulnerability_severity: "Critical" # Minimum severity that will cause the artefact to not be promoted
    promote: true # Whether to promote artefacts or not
    delete: true # Whether to delete staging artefact from source repository

  nexus:
    quarantine: true # Whether to quarantine artefacts
    delete: true # Whether to delete artefact from source repository

  metrics:
    prometheus_multiproc_dir: "/metrics-registry" # Target folder for prometheus metrics
```
The configuration follows a pydantic module defined in each module.

## Metrics

The `metrics_manager.py` module is used for managing and reporting Prometheus-based metrics. It facilitates the monitoring of various application-specific metrics through a centralized system.

### Adding and using Metrics
To add a new metric, define it within the `__init__` method of the `MetricsManager`. Then, use the metric by referencing it through the MetricsManager instance across your application.

#### Defining a new metric
```python
class MetricsManager(object):
    ...
    def __init__(self):
            ...
            self.new_operation_count = Counter(
                "new_operation_count",
                "Counts each new operation processed.",
                ["operation_type"]
            )

            # Do not forget to initialize the label values
            self.new_operation_count.labels(operation_type="type1")
```

#### Using the metric
```python
from ska_cicd_artefact_validations.models.metrics.metrics_manager import (
    metrics,
)

metrics.increment_counter(metrics.new_operation_count, amount=1, operation_type="type1")
```

### Feature Toggle for Metrics
Metrics collection can be toggled using the feature flag `metrics` stored in the GitLab repository under `Deploy > Feature flags`. This flag allows for the dynamic enabling or disabling of metrics collection.

### Accessing Metrics
Metrics are exposed through the `/metrics`endpoint, which can be accessed at `http://<host>:<port>/metrics`


### Available Metrics
<table>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Labels</th>
    </tr>
    <tr>
        <td>check_runtime_seconds</td>
        <td>Summary metric that tracks the time taken to run each type of check.</td>
        <td>check_name: The name of the check being timed.</td>
    </tr>
    <tr>
        <td>num_checks_run</td>
        <td>Counter that records the total number of checks executed.</td>
        <td></td>
    </tr>
    <tr>
        <td>num_checks_failed</td>
        <td>Counter that records the number of checks that have failed.</td>
        <td>check_name: The name of the check that failed.</td>
    </tr>
    <tr>
        <td>num_uploaded_artefacts</td>
        <td>Counter that tracks the number of artefacts uploaded during operations.</td>
        <td></td>
    </tr>
    <tr>
        <td>num_exceptions</td>
        <td>Counter that logs the number of exceptions raised during function executions.</td>
        <td>function_name: The name of the function where the exception occurred.</td>
    </tr>

<table>

## Future contributions
### Artefact registries and artefact types:
Should the SKAO project move to hosting Helm artefacts in Harbor, the Harbor handler should be restructured to something similar to the Nexus handler, utilizing a base handler class that can be extended by each artefact handler.

### Integration tests
Integration tests should be developed to interact with staging versions of Harbor and Nexus, to facilitate a live scenario. Tests should trigger validations for every type of artefact and test both success and failure scenarios.

### Notifying the users
Currently, the only way of notifying that the artefact is not compliant is through a merge request. This isn't immediately noticeable by the developer. To complement this, the service can move to use the People API to send targetted Slack notifications to the developer, with the caveat that the required data to identify the user comes from the artefact metadata. If the metadata is not present, a notification is impossible.